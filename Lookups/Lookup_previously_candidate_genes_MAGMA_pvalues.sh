#################################################################################################################
# By Else Eising, 19-1-2021
# Script to lookup previously published candidate gene hits
#################################################################################################################

# Define paths:
DataDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_after_metaanalysis/FUMA/"
ResultsDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Lookups/Previously_published_candidate_genes"


##############################################################################################################
## Step 1: Lookups
##############################################################################################################

# Use main GWAS results

# WR
head -n 1 $DataDir/FUMA_job122687_WR_Z/magma.genes.out > $ResultsDir/FUMA_job122687_WR_ZPreviously_published_candidate_genes.txt
while read file2; do
	grep $file2$ $DataDir/FUMA_job122687_WR_Z/magma.genes.out 
	grep $file2$ $DataDir/FUMA_job122687_WR_Z/magma.genes.out >> $ResultsDir/FUMA_job122687_WR_ZPreviously_published_candidate_genes.txt
done < $ResultsDir/Previously_published_candidates.txt

# NREAD
head -n 1 $DataDir/FUMA_job122690_NREAD_RT/magma.genes.out > $ResultsDir/FUMA_job122690_NREAD_RTPreviously_published_candidate_genes.txt
while read file2; do
	grep $file2$ $DataDir/FUMA_job122690_NREAD_RT/magma.genes.out 
	grep $file2$ $DataDir/FUMA_job122690_NREAD_RT/magma.genes.out >> $ResultsDir/FUMA_job122690_NREAD_RTPreviously_published_candidate_genes.txt
done < $ResultsDir/Previously_published_candidates.txt


# PA
head -n 1 $DataDir/FUMA_job123932_PA_RT/magma.genes.out > $ResultsDir/FUMA_job123932_PA_RTPreviously_published_candidate_genes.txt
while read file2; do
	grep $file2$ $DataDir/FUMA_job123932_PA_RT/magma.genes.out 
	grep $file2$ $DataDir/FUMA_job123932_PA_RT/magma.genes.out >> $ResultsDir/FUMA_job123932_PA_RTPreviously_published_candidate_genes.txt
done < $ResultsDir/Previously_published_candidates.txt

# SP
head -n 1 $DataDir/FUMA_job122689_SP_RT/magma.genes.out > $ResultsDir/FUMA_job122689_SP_RTPreviously_published_candidate_genes.txt
while read file2; do
	grep $file2$ $DataDir/FUMA_job122689_SP_RT/magma.genes.out 
	grep $file2$ $DataDir/FUMA_job122689_SP_RT/magma.genes.out >> $ResultsDir/FUMA_job122689_SP_RTPreviously_published_candidate_genes.txt
done < $ResultsDir/Previously_published_candidates.txt

# NREP
head -n 1 $DataDir/FUMA_job122693_NREP_Z/magma.genes.out > $ResultsDir/FUMA_job122693_NREP_ZPreviously_published_candidate_genes.txt
while read file2; do
	grep $file2$ $DataDir/FUMA_job122693_NREP_Z/magma.genes.out 
	grep $file2$ $DataDir/FUMA_job122693_NREP_Z/magma.genes.out >> $ResultsDir/FUMA_job122693_NREP_ZPreviously_published_candidate_genes.txt
done < $ResultsDir/Previously_published_candidates.txt
