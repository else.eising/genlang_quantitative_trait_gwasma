#################################################################################################################
# By Else Eising, 19-1-2021
# Script to lookup previously published GWAS hits
#################################################################################################################

# Define paths:
DataDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/"
ResultsDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Lookups/Previously_published_GWAS_hits"


##############################################################################################################
## Step 1: Lookups
##############################################################################################################

# Use main GWAS results

echo "WR_Z/METAANALYSIS_WR_Z_combined_STERR_GCON_1_chrpos.txt
NREAD_RT/METAANALYSIS_NREAD_RT_combined_STERR_GCON_1_chrpos.txt
SP_RT/METAANALYSIS_SP_RT_combined_STERR_GCON_1_chrpos.txt
PA_RT/METAANALYSIS_PA_RT_combined_STERR_GCON_1_chrpos.txt
NREP_Z/METAANALYSIS_RANDOM__NREP_Z_combined_STERR_GCON_1_chrpos.txt" > $ResultsDir/Main_GWAS_sumstat_files.txt

while read file; do
	outfile=$(echo $file | sed 's|^[A-Z]*_RT/METAANALYSIS|Previously_published_GWAS_hits|g' | sed 's|^[A-Z]*_Z/METAANALYSIS|Previously_published_GWAS_hits|g' | sed 's|_combined_STERR_GCON_1_chrpos||g' )
	echo $outfile
	head -n 1 $DataDir/$file > $ResultsDir/$outfile
	awk 'BEGIN{OFS="\t"} FNR==NR{a[$1]=$0; next} {print a[$1]}' $DataDir/$file $ResultsDir/Previously_published_GWAS_hits.txt >> $ResultsDir/$outfile
done < $ResultsDir/Main_GWAS_sumstat_files.txt


##############################################################################################################
## Step 2: lookups without original cohort
##############################################################################################################

# spelling rs1555839 Truong 2019
head -n 1 $DataDir/SP_RT/METAANALYSIS_SP_RT_combined_STERR_GCON_noGRaD_1_chrpos.txt > $ResultsDir/METAANALYSIS_SP_RT_combined_STERR_GCON_noGRaD_1_chrpos_rs1555839.txt
grep rs1555839 $DataDir/SP_RT/METAANALYSIS_SP_RT_combined_STERR_GCON_noGRaD_1_chrpos.txt >> $ResultsDir/METAANALYSIS_SP_RT_combined_STERR_GCON_noGRaD_1_chrpos_rs1555839.txt

# Nonword reading rs6035856 Gialluisi 2020
# GenLang without Neurodys, CLDRC and UKdys
head -n 1 $DataDir/NREAD_RT/METAANALYSIS_NREAD_RT_combined_STERR_GCON_noCLDRCnoNEURODYSnoUKDYS_1.tbl > $ResultsDir/METAANALYSIS_NREAD_RT_combined_STERR_GCON_noCLDRCnoNEURODYSnoUKDYS_rs6035856.txt
grep rs6035856 $DataDir/NREAD_RT/METAANALYSIS_NREAD_RT_combined_STERR_GCON_noCLDRCnoNEURODYSnoUKDYS_1.tbl >> $ResultsDir/METAANALYSIS_NREAD_RT_combined_STERR_GCON_noCLDRCnoNEURODYSnoUKDYS_rs6035856.txt

# Nonword repetition rs2192161 Luciano 2013
# GenLang without ALSPAC and BATS
head -n 1 $DataDir/NREP_Z/METAANALYSIS_RANDOM__NREP_Z_combined_STERR_GCON_noALSPACnoBATS_1.tbl > $ResultsDir/METAANALYSIS_RANDOM__NREP_Z_combined_STERR_GCON_noALSPACnoBATS_rs2192161.txt
grep rs2192161 $DataDir/NREP_Z/METAANALYSIS_RANDOM__NREP_Z_combined_STERR_GCON_noALSPACnoBATS_1.tbl >> $ResultsDir/METAANALYSIS_RANDOM__NREP_Z_combined_STERR_GCON_noALSPACnoBATS_rs2192161.txt


