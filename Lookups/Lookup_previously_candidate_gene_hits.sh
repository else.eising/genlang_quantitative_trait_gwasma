#################################################################################################################
# By Else Eising, 19-1-2021
# Script to lookup previously published candidate gene hits
#################################################################################################################

# Define paths:
DataDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/"
ResultsDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Lookups/Previously_published_candidate_gene_hits"


##############################################################################################################
## Step 1: Lookups
##############################################################################################################

# Use main GWAS results

echo "WR_Z/METAANALYSIS_WR_Z_combined_STERR_GCON_1_chrpos.txt
NREAD_RT/METAANALYSIS_NREAD_RT_combined_STERR_GCON_1_chrpos.txt
SP_RT/METAANALYSIS_SP_RT_combined_STERR_GCON_1_chrpos.txt
PA_RT/METAANALYSIS_PA_RT_combined_STERR_GCON_1_chrpos.txt
NREP_Z/METAANALYSIS_RANDOM__NREP_Z_combined_STERR_GCON_1_chrpos.txt" > $ResultsDir/Main_GWAS_sumstat_files.txt

while read file; do
	outfile=$(echo $file | sed 's|^[A-Z]*_RT/METAANALYSIS|Previously_published_candidate_gene_hits|g' | sed 's|^[A-Z]*_Z/METAANALYSIS|Previously_published_candidate_gene_hits|g' | sed 's|_combined_STERR_GCON_1_chrpos||g' )
	echo $outfile
	head -n 1 $DataDir/$file > $ResultsDir/$outfile
	awk 'BEGIN{OFS="\t"} FNR==NR{a[$1]=$0; next} {print a[$1]}' $DataDir/$file $ResultsDir/Previously_published_candidate_gene_hits.txt >> $ResultsDir/$outfile
done < $ResultsDir/Main_GWAS_sumstat_files.txt



