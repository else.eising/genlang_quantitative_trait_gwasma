# By Else Eising, 1 March 2018
# Script to check whether the log files produced by METAL contain error messages


#-------------------------------------------------------------------
# Check for warnings on meta analysis log file
#-------------------------------------------------------------------

# make a list of all folders on the cluster file system containing METAL output
ls /data/clusterfs/lag/users/elseis/GenLang/ | grep -E '_RT|_Z' > /data/clusterfs/lag/users/elseis/GenLang/List_of_meta_analyses.txt

# read all logs in each folder, report any mentions of "ERROR" and "Failed"
while read Pheno; do 
cd /data/clusterfs/lag/users/elseis/GenLang/"$Pheno"/
echo ${Pheno}
grep 'Failed' *.log
grep 'ERROR' *.log
done < /data/clusterfs/lag/users/elseis/GenLang/List_of_meta_analyses.txt

