## Script to select a part of the meta-analysis results for Locuszoom
# By Else Eising

# -------------------------------------
# Define paths
# -------------------------------------

DataDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/"
ResultsDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_after_metaanalysis/Locuszoom/"


# -------------------------------------
# Select regions
# -------------------------------------

# select a significant region from the summary statistics
# NR==1 prints the header (the first row)
# in awk, the $ sign indicates the column number
# so NR>1 && $2 == 10 && $3 >= 104128873 && $3 <= 105128873)' gives you the region on chromosome 10 between base pairs 104128873 and 105128873
# then it will print column 1 and 8 to a new file

# WR_RT, rs11208009
# chr1:63195692 +/- 1 million bp
awk '(NR==1 || NR>1 && $17 == 1 && $18 >= 62195692 && $18 <= 64195692){print $1 "\t" $8 "\t" $10}' $DataDir/WR_RT/METAANALYSIS_WR_RT_combined_STERR_GCON_1_chrpos.txt > $ResultsDir/WR_RT_rs11208009.txt

# WR_Z, rs11208009
# chr1:63195692 +/- 1 million bp
awk '(NR==1 || NR>1 && $18 == 1 && $19 >= 62195692 && $19 <= 64195692 && $16 > 5000) {print $1 "\t" $8 "\t" $10}' $DataDir/WR_Z/METAANALYSIS_WR_Z_combined_STERR_GCON_1_chrpos.txt > $ResultsDir/WR_Z_rs11208009.txt

