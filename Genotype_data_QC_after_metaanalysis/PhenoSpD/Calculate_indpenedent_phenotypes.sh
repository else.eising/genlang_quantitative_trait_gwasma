#----------------------------------------------------------------------------------------------------------
# script for the calculation of the number of independent phenotypes in GenLang
# by Else Eising
# The script uses PhenoSpD
# see https://github.com/MRCIEU/PhenoSpD and https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6109640/#sup6
#----------------------------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------------------------
# Set directories
#----------------------------------------------------------------------------------------------------------

PhenospdDir="/home/elseis/PhenoSpD/"
LDSCdir="/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_after_metaanalysis/LDSC/"
PhenospdOuptupDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_after_metaanalysis/PhenoSpD/"

#----------------------------------------------------------------------------------------------------------
# Install and update PhenoSpD
#----------------------------------------------------------------------------------------------------------

mkdir $PhenospdDir
cd /home/elseis/
git clone https://github.com/MRCIEU/PhenoSpD.git
cd $PhenospdDir
git pull


#----------------------------------------------------------------------------------------------------------
# Run PhenoSpD on LDSC correlation matrix
#----------------------------------------------------------------------------------------------------------



cd $PhenospdDir
## for the GenLang traits
Rscript ./script/phenospd.r --phenocorr $LDSCdir/RT_combined/Genetic_correlation_RT_combined_noColAndRownames_forPhenoSpD.txt --out $PhenospdOuptupDir/Correlation_estimate_RT_combined_LDSC


## for the GenLang traits and external cognitive sumstats (Fig 1)
Rscript ./script/phenospd.r --phenocorr $LDSCdir/RT_combined_external_sumstats/Genetic_correlation_RT_combined_external_sumstats_noColAndRownames_forPhenoSpD.txt --out $PhenospdOuptupDir/Correlation_estimate_RT_combined_external_sumstats_LDSC

