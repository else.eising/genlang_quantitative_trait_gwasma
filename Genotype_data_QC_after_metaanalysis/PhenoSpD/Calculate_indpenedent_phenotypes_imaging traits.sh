#----------------------------------------------------------------------------------------------------------
# script for the calculation of the number of independent phenotypes in GenLang
# by Else Eising
# The script uses PhenoSpD
# see https://github.com/MRCIEU/PhenoSpD and https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6109640/#sup6
#----------------------------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------------------------
# Set directories
#----------------------------------------------------------------------------------------------------------

PhenospdDir="/home/elseis/PhenoSpD/"
LDSCdir="/data/workspaces/lag/workspaces/lg-genlang/Working/UKBB/gencor_imaging/"
PhenospdOuptupDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_after_metaanalysis/PhenoSpD/"


#----------------------------------------------------------------------------------------------------------
# Run PhenoSpD on LDSC correlation matrix
#----------------------------------------------------------------------------------------------------------


cd $PhenospdDir
##using the existing LD Hub phenotypic correlation
<<<<<<< HEAD
Rscript ./script/phenospd.r --phenocorr $LDSCdir/Genetic_correlation_imaging_traits_no_volume_noColAndRownames_forPhenoSpD.txt --out $PhenospdOuptupDir/Correlation_estimate_imaging_traits
=======
Rscript ./script/phenospd.r --phenocorr $LDSCdir/Genetic_correlation_imaging_traits_noColAndRownames_forPhenoSpD.txt --out $PhenospdOuptupDir/Correlation_estimate_imaging_traits
>>>>>>> 6890149... first commit

