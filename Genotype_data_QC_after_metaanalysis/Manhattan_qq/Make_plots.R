#----------------------------------------------------------------------------------------------------------
# script to makes a QQ-plot and Manhattan plot of the sumstats
# by Else Eising
#----------------------------------------------------------------------------------------------------------

# to submit this script to the cluster, use the script Make_plots.sh
# give as argument the folder that contains data you want to plot!

#----------------------------------------------------------------------------------------------------------
# Load libraries
#----------------------------------------------------------------------------------------------------------

library(qqman) # for manhattan plot 
library(stringr)
require(data.table) # for fread
library(stats)
library(reshape2)
library(rlang)
library(lattice) # for qqplot with function below)
library(grid)
library(viridis)


#----------------------------------------------------------------------------------------------------------
# QQplot function
# from https://genome.sph.umich.edu/wiki/Code_Sample:_Generating_QQ_Plots_in_R by Matthey Flickinger
#----------------------------------------------------------------------------------------------------------

qqunif.plot<-function(pvalues, 
	should.thin=T, thin.obs.places=2, thin.exp.places=2, 
	xlab=expression(paste("Expected (",-log[10], " p-value)")),
	ylab=expression(paste("Observed (",-log[10], " p-value)")), 
	draw.conf=TRUE, conf.points=1000, conf.col="lightgray", conf.alpha=.05,
	already.transformed=FALSE, pch=20, aspect="iso", prepanel=prepanel.qqunif,
	par.settings=list(superpose.symbol=list(pch=pch)), ...) {
	
	
	#error checking
	if (length(pvalues)==0) stop("pvalue vector is empty, can't draw plot")
	if(!(class(pvalues)=="numeric" || 
		(class(pvalues)=="list" && all(sapply(pvalues, class)=="numeric"))))
		stop("pvalue vector is not numeric, can't draw plot")
	if (any(is.na(unlist(pvalues)))) stop("pvalue vector contains NA values, can't draw plot")
	if (already.transformed==FALSE) {
		if (any(unlist(pvalues)==0)) stop("pvalue vector contains zeros, can't draw plot")
	} else {
		if (any(unlist(pvalues)<0)) stop("-log10 pvalue vector contains negative values, can't draw plot")
	}
	
	
	grp<-NULL
	n<-1
	exp.x<-c()
	if(is.list(pvalues)) {
		nn<-sapply(pvalues, length)
		rs<-cumsum(nn)
		re<-rs-nn+1
		n<-min(nn)
		if (!is.null(names(pvalues))) {
			grp=factor(rep(names(pvalues), nn), levels=names(pvalues))
			names(pvalues)<-NULL
		} else {
			grp=factor(rep(1:length(pvalues), nn))
		}
		pvo<-pvalues
		pvalues<-numeric(sum(nn))
		exp.x<-numeric(sum(nn))
		for(i in 1:length(pvo)) {
			if (!already.transformed) {
				pvalues[rs[i]:re[i]] <- -log10(pvo[[i]])
				exp.x[rs[i]:re[i]] <- -log10((rank(pvo[[i]], ties.method="first")-.5)/nn[i])
			} else {
				pvalues[rs[i]:re[i]] <- pvo[[i]]
				exp.x[rs[i]:re[i]] <- -log10((nn[i]+1-rank(pvo[[i]], ties.method="first")-.5)/(nn[i]+1))
			}
		}
	} else {
		n <- length(pvalues)+1
		if (!already.transformed) {
			exp.x <- -log10((rank(pvalues, ties.method="first")-.5)/n)
			pvalues <- -log10(pvalues)
		} else {
			exp.x <- -log10((n-rank(pvalues, ties.method="first")-.5)/n)
		}
	}


	#this is a helper function to draw the confidence interval
	panel.qqconf<-function(n, conf.points=1000, conf.col="gray", conf.alpha=.05, ...) {
		require(grid)
		conf.points = min(conf.points, n-1);
		mpts<-matrix(nrow=conf.points*2, ncol=2)
        	for(i in seq(from=1, to=conf.points)) {
            		mpts[i,1]<- -log10((i-.5)/n)
            		mpts[i,2]<- -log10(qbeta(1-conf.alpha/2, i, n-i))
            		mpts[conf.points*2+1-i,1]<- -log10((i-.5)/n)
            		mpts[conf.points*2+1-i,2]<- -log10(qbeta(conf.alpha/2, i, n-i))
        	}
        	grid.polygon(x=mpts[,1],y=mpts[,2], gp=gpar(fill=conf.col, lty=0), default.units="native")
    	}

	#reduce number of points to plot
	if (should.thin==T) {
		if (!is.null(grp)) {
			thin <- unique(data.frame(pvalues = round(pvalues, thin.obs.places),
				exp.x = round(exp.x, thin.exp.places),
				grp=grp))
			grp = thin$grp
		} else {
			thin <- unique(data.frame(pvalues = round(pvalues, thin.obs.places),
				exp.x = round(exp.x, thin.exp.places)))
		}
		pvalues <- thin$pvalues
		exp.x <- thin$exp.x
	}
	gc()
	
	prepanel.qqunif= function(x,y,...) {
		A = list()
		A$xlim = range(x, y)*1.02
		A$xlim[1]=0
		A$ylim = A$xlim
		return(A)
	}

	#draw the plot
	xyplot(pvalues~exp.x, groups=grp, xlab=xlab, ylab=ylab, aspect=aspect, col=viridis(n=3)[2],
		prepanel=prepanel, scales=list(axs="i"), pch=pch,
		panel = function(x, y, ...) {
			if (draw.conf) {
				panel.qqconf(n, conf.points=conf.points, 
					conf.col=conf.col, conf.alpha=conf.alpha)
			};
			panel.xyplot(x,y, ...);
			panel.abline(0,1);
		}, par.settings=par.settings, ...
	)
}

#----------------------------------------------------------------------------------------------------------
# Read in argument (= directory)
#----------------------------------------------------------------------------------------------------------

##First read in the arguments listed at the command line
args = commandArgs(trailingOnly=TRUE)

##args is now a list of character vectors
## First check to see if arguments are passed.
## Then cycle through each element of the list and evaluate the expressions.
if(length(args) < 1){
    stop("No or too few arguments supplied. Give directory name as arguments!")
}
if(length(args) > 1) {
	stop("Too many arguments supplied. Give only directory name")
}
directory <- eval(parse(text=rlang::quo_name(quote(args[[1]]))))
print(directory)

#----------------------------------------------------------------------------------------------------------
# Correction for multiple testing
#----------------------------------------------------------------------------------------------------------

# PhenoSpD was used to calculate the number of independent phenotypes
IndependentTraits <- 2.1504
SigThres = 5e-8 / IndependentTraits

#----------------------------------------------------------------------------------------------------------
# read in meta-analysis results
#----------------------------------------------------------------------------------------------------------

setwd(paste("/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/", directory, sep=""))
getwd()

# Get list of files to plot
files <- list.files(path=getwd(), pattern="*_1_chrpos.txt$")
files_fixed <- files[grep(paste("METAANALYSIS_", directory, sep=""), files)]

# read in files and make plots
for(sumstat in files_fixed) {
	print(sumstat)
	print("reading in data")
	Results <- fread(sumstat, header=TRUE, sep="\t", fill=TRUE)
	Results2 <- Results[!is.na(Results$MarkerName),]
	Results2$CHR <- as.numeric(gsub("X","23",Results2$CHR))
	Results2$POS <- as.numeric(Results2$POS)
	lower_limit <- max(Results2$TotalSampleSize)/2
	Results4 <- Results2[Results2$TotalSampleSize >= lower_limit,]
	print("make qqplot of heterogeneity p-value")
	filename <- gsub(".txt", "_qqplot_heterogeneity.png", sumstat)
	png(filename, type="cairo", width = 14.1, height = 14.1, units="cm", res=600)
	plot1 <- qqunif.plot(Results4$HetPVal)
	print(plot1)
	dev.off()
	print("making normal qqplot")
	filename <- gsub(".txt", "_qqplot.png", sumstat)
	png(filename, type="cairo", width = 14.1, height = 14.1, units="cm", res=600)
	plot2 <- qqunif.plot(Results4$"P-value")
	print(plot2)
	dev.off()
	print("make manhattan plot")
	filename <- gsub(".txt", "_manhattanplot.png", sumstat)
	png(filename, type="cairo", width = 15, height = 4, units="in", res=900)
	manhattan(Results4, chr = "CHR", bp = "POS", p = "P-value", snp = "MarkerName", col = viridis(n=23), chrlabs = NULL, suggestiveline = -log10(5e-08), genomewideline = -log10(SigThres), highlight = NULL, logp = TRUE, annotatePval = NULL, annotateTop = NULL, cex.axis=0.7, cex.lab=0.7, cex=0.7)
	dev.off()
	}

files_random <- files[grep("RANDOM", files)]

# read in files and make plots
for(sumstat in files_random) {
	print(sumstat)
	print("reading in data")
	Results <- fread(sumstat, header=TRUE, sep="\t", fill=TRUE)
	Results2 <- Results[!is.na(Results$MarkerName),]
	Results2$CHR <- as.numeric(gsub("X","23",Results2$CHR))
	Results2$POS <- as.numeric(Results2$POS)
	lower_limit <- max(Results2$TotalSampleSize)/2
	Results4 <- Results2[Results2$TotalSampleSize >= lower_limit,]
	print("make qqplot of heterogeneity p-value")
	filename <- gsub(".txt", "_qqplot_heterogeneity.png", sumstat)
	png(filename, type="cairo", width = 14.1, height = 14.1, units="cm", res=600)
	plot0<-qqunif.plot(Results4$HetPVal)
	print(plot0)
	dev.off()
	print("making normal qqplot of fixed effects meta-analysis results")
	filename <- gsub(".txt", "_qqplot_fixed.png", sumstat)
	png(filename, type="cairo", width = 14.1, height = 14.1, units="cm", res=600)
	plot1<-qqunif.plot(Results4$Pvalue)
	print(plot1)
	dev.off()
	print("making normal qqplot")
	filename <- gsub(".txt", "_qqplot_random.png", sumstat)
	png(filename, type="cairo", width = 14.1, height = 14.1, units="cm", res=600)
	plot2<-qqunif.plot(Results4$PvalueARE)
	print(plot2)
	dev.off()
	print("make manhattan plot")
	filename <- gsub(".txt", "_manhattanplot_random.png", sumstat)
	png(filename, type="cairo", width = 15, height = 4, units="in", res=900)
	manhattan(Results4, chr = "CHR", bp = "POS", p = "PvalueARE", snp = "MarkerName", col = viridis(n=23), chrlabs = NULL, suggestiveline = -log10(5e-08), genomewideline = -log10(SigThres), highlight = NULL, logp = TRUE, annotatePval = NULL, annotateTop = NULL)
	dev.off()
	filename <- gsub(".txt", "_manhattanplot_fixed.png", sumstat)
	png(filename, type="cairo", width = 15, height = 4, units="in", res=900)
	manhattan(Results4, chr = "CHR", bp = "POS", p = "Pvalue", snp = "MarkerName", col = viridis(n=23), chrlabs = NULL, suggestiveline = -log10(5e-08), genomewideline = -log10(SigThres), highlight = NULL, logp = TRUE, annotatePval = NULL, annotateTop = NULL, cex.axis=0.7, cex.lab=0.7, cex=0.7)
	dev.off()
	}

