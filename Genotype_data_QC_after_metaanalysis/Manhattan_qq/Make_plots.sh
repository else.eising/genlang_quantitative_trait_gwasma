#\!/bin/sh
#$ -N Plot_meta_analysis_results_GenLang
#$ -o /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_after_metaanalysis/Manhattan_qq_sensitivity/logs
#$ -e /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_after_metaanalysis/Manhattan_qq_sensitivity/logs
#$ -cwd
#$ -q single15.q
#$ -S /bin/bash
#$ -M else.eising@mpi.nl
#$ -m beas

#-----------
# Script to submit the script Make_plots.R to the grid

# submit this script with the phenotype as argument
# e.g.: qsub Make_plots.sh WR_Z

# For-loop to submit the script with all phenotypes:
# for pheno in NREAD_RT NREP_Z NREP_RT PA_RT SP_RT PIQ_Z PIQ_RT WR_Z WR_RT 
# for pheno in NREAD_RT_MALE NREP_Z_MALE NREP_RT_MALE PA_RT_MALE SP_RT_MALE PIQ_Z_MALE PIQ_RT_MALE WR_Z_MALE WR_RT_MALE
# for pheno in NREAD_RT_FEMALE NREP_Z_FEMALE NREP_RT_FEMALE PA_RT_FEMALE SP_RT_FEMALE PIQ_Z_FEMALE PIQ_RT_FEMALE WR_Z_FEMALE WR_RT_FEMALE
# do
 # qsub Make_plots.sh $pheno
# done
#--------------

echo $1

cd /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_after_metaanalysis/Manhattan_qq_sensitivity

Rscript Make_plots.R $1

