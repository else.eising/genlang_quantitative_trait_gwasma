#\!/bin/sh
#$1 - name of folder in /data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/

# By Else Eising
# Script to reformat sumstats for Locuszoom and FUMA

#-------------------------------------------------------------------
cd /data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/$1

# make list of files to annotate
ls | grep _GCON_1_chrpos.txt$ | grep METAANALYSIS_$1 > List_of_meta_analysis_results.txt
ls | grep _GCOFF_1_chrpos.txt$ | grep METAANALYSIS_$1 >> List_of_meta_analysis_results.txt
ls | grep _GCON_1_chrpos.txt$ | grep METAANALYSIS_RANDOM > List_of_meta_analysis_results_random_effects.txt
ls | grep _GCOFF_1_chrpos.txt$ | grep METAANALYSIS_RANDOM >> List_of_meta_analysis_results_random_effects.txt

# For Locuszoom and FUMA:
# 1) make header
# 2) filter on sample size: 5000
# 3) Print rsID, PVAL and sample size to new file 

## Note: FUMA will give error when header contains "-" (e.g. in P-value, the standard output of Metal!!!)


while read file; do
outfile=$(echo $file| sed 's|_chrpos.txt|_chrpos_forLocuszoom_and_FUMA.txt|g')
echo $file 
echo -e "RSID\tP\tN" > $outfile
tail -n +2 $file | awk 'BEGIN{OFS="\t"} {if ($16 > 5000) {print $1, $10, $16}}'  >> $outfile
done < List_of_meta_analysis_results.txt

while read file; do
outfile=$(echo $file| sed 's|_chrpos.txt|_chrpos_forLocuszoom_and_FUMA.txt|g')
echo $file 
echo -e "RSID\tP\tN" > $outfile
tail -n +2 $file | awk 'BEGIN{OFS="\t"} {if ($22 > 5000) {print $1, $18, $22}}'  >> $outfile
done < List_of_meta_analysis_results_random_effects.txt


rm tmp
