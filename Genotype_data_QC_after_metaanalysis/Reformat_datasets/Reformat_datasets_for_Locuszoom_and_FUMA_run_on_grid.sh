#\!/bin/sh
#$ -N Prep_files_Locuszoom_FUMA
#$ -o /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_after_metaanalysis/Prepare_datasets/logs
#$ -e /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_after_metaanalysis/Prepare_datasets/logs
#$ -cwd
#$ -q single.q
#$ -S /bin/bash
#$ -M else.eising@mpi.nl
#$ -m beas

cd /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_after_metaanalysis/Prepare_datasets
./Reformat_datasets_for_Locuszoom_and_FUMA.sh WR_RT
./Reformat_datasets_for_Locuszoom_and_FUMA.sh WR_Z
./Reformat_datasets_for_Locuszoom_and_FUMA.sh NREAD_RT
./Reformat_datasets_for_Locuszoom_and_FUMA.sh PA_RT
./Reformat_datasets_for_Locuszoom_and_FUMA.sh SP_RT
./Reformat_datasets_for_Locuszoom_and_FUMA.sh NREP_Z
./Reformat_datasets_for_Locuszoom_and_FUMA.sh NREP_RT
./Reformat_datasets_for_Locuszoom_and_FUMA.sh PIQ_RT

