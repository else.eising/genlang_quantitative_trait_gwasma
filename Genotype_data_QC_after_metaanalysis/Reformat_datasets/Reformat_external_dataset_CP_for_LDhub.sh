# By Else Eising
# Script to format external sumstats for LDhub

#-------------------------------------------------------------------
# Add chromosome and position to meta-analysis results based on rs ID
#-------------------------------------------------------------------

LDhubDir=/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_after_metaanalysis/LDHub/

cd /data/workspaces/lag/workspaces/lg-genlang/Working/External_sumstats/Lee_2018_EA

# For LDhub
# 1) make header
# 2) merge with LDHub SNP list
# 3) filter on sample size: 5000
# 4) Print rsID, A1, A2, Zscore (effect/stdev), N an PVAL


echo -e "RSID\tA1\tA2\tBETA\tN\tP" > GWAS_CP_all_forLDhub.txt
awk 'BEGIN{OFS="\t"} FNR==NR{a[$1]=$1 OFS $4 OFS $5 OFS $7 OFS $8 OFS $9; next} $1 in a {print a[$1]}' GWAS_CP_all.txt $LDhubDir/w_hm3.noMHC.snplist > tmp
awk 'BEGIN{OFS="\t"} {print $1, $2, $3, $4/$5, "257828", $6}' tmp >> GWAS_CP_all_forLDhub.txt
zip GWAS_CP_all_forLDhub.zip GWAS_CP_all_forLDhub.txt

rm tmp
