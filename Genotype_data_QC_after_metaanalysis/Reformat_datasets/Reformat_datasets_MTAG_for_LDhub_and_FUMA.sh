# By Else Eising
# Script to prepare output files of MTAG for LDhub and FUMA

#-------------------------------------------------------------------
# Add chromosome and position to meta-analysis results based on rs ID
#-------------------------------------------------------------------

LDhubDir=/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_after_metaanalysis/LDHub/

cd /data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_MTAG/

# make list of files to annotate
ls | grep _trait_[1-9].txt$ > List_of_meta_analysis_results.txt
# or:
ls | grep _trait_1.txt$ > List_of_meta_analysis_results.txt

# For FUMA:
# 1) make header
# 2) filter on sample size: 5000
# 3) Print rsID, PVAL, CHR, POS and N to new file

# For LDhub
# 1) make header
# 2) merge with LDHub SNP list
# 3) filter on sample size: 5000
# 4) Print rsID, A1, A2, Zscore (effect/stdev), N an PVAL

while read file; do
outfile=$(echo $file| sed 's|.txt|_forFUMA.txt|g')
echo $file 
echo -e "RSID\tPVAL" > $outfile
awk 'BEGIN{OFS="\t"} NR>1{print $1, $7}' $file >> $outfile
outfile2=$(echo $file| sed 's|.txt|_forLDhub.txt|g')
echo -e "RSID\tA1\tA2\tZSCORE\tN\tPVAL" > $outfile2
awk 'BEGIN{OFS="\t"} FNR==NR{a[$1]=$1 OFS $4 OFS $5 OFS $11 OFS $7 OFS $12; next} $1 in a {print a[$1]}' $file $LDhubDir/w_hm3.noMHC.snplist | tail -n +2 >> $outfile2
zip $outfile2.zip $outfile2
done < List_of_meta_analysis_results.txt

