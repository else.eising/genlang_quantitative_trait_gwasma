#\!/bin/sh
#$ -N Add_chr_pos
#$ -o /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_after_metaanalysis/Prepare_datasets/logs
#$ -e /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_after_metaanalysis/Prepare_datasets/logs
#$ -cwd
#$ -q single.q
#$ -S /bin/bash
#$ -M else.eising@mpi.nl
#$ -m beas


# By Else Eising
# Script to add chromosome and position to METAL output files

#-------------------------------------------------------------------
# submit this script with the phenotype as argument
#-------------------------------------------------------------------

# cd /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_after_metaanalysis/Prepare_datasets
# e.g.: qsub Prepare_datasets_add_CHR_POS.sh WR_Z

# For-loop to submit the script with all phenotypes:
# for pheno in NREAD_RT NREP_Z NREP_RT PA_RT SP_RT PIQ_Z PIQ_RT WR_Z WR_RT 
# for pheno in NREAD_RT_MALE NREP_Z_MALE NREP_RT_MALE PA_RT_MALE SP_RT_MALE PIQ_Z_MALE PIQ_RT_MALE WR_Z_MALE WR_RT_MALE
# for pheno in NREAD_RT_FEMALE NREP_Z_FEMALE NREP_RT_FEMALE PA_RT_FEMALE SP_RT_FEMALE PIQ_Z_FEMALE PIQ_RT_FEMALE WR_Z_FEMALE WR_RT_FEMALE
# do
 # qsub Prepare_datasets_add_CHR_POS.sh $pheno
# done


#-------------------------------------------------------------------
# Add chromosome and position to meta-analysis results based on rs ID
#-------------------------------------------------------------------

HRCdir=/home/elseis/HRC-1000G-check-bim-v4.2.7/

cd /data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/$1

# make list of files to annotate
ls | grep GCOFF_1.tbl$ > List_of_meta_analysis_results.txt
ls | grep GCON | grep _1.tbl$ >> List_of_meta_analysis_results.txt

# For each file:
# 1) make header
# 2) add chromosome, position and chr:pos from HRC file
# 3) store top results in separate file

while read file; do
outfile=$(echo $file| sed 's|.tbl|_chrpos.txt|g')
echo $file 
head -n 1 $file | awk 'BEGIN{OFS="\t"} {print $0, "CHR","POS", "chrposID"}' > $outfile
awk 'BEGIN{OFS="\t"} NR==FNR{a[$3]=$1 OFS $2 OFS $1 ":" $2; next} {print $0, a[$1]}' $HRCdir/HRC.r1-1.GRCh37.wgs.mac5.sites.tab $file | tail -n +2 >> $outfile
outfile2=$(echo $file| sed 's|.tbl|_chrpos_pval_e05_topresults.txt|g')
head -n 1 $outfile > $outfile2
awk '{if ($10 < 0.00001) {print $0}}' $outfile >> $outfile2
done < List_of_meta_analysis_results.txt

