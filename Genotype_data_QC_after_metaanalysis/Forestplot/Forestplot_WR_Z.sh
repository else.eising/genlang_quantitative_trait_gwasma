# By Else Eising
# Script to collect results for the Forest plot, and then produce a Forest plot in R

DataDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/"
MetaDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/"
WorkingDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/WR_Z/Forestplot/"

[[ -d $WorkingDir ]] || mkdir $WorkingDir
cd $WorkingDir

#-------------------------------------------------------------------
# prepare files, in linux
#-------------------------------------------------------------------

# for each locus, collect GWAS results
# also make a file for the meta-analysis results. This is a separate file, as the headers differ

head -n 1 $MetaDir/WR_Z/METAANALYSIS_WR_Z_combined_STERR_GCON_1_chrpos.txt > $WorkingDir/Forestplot_details.METAANALYSIS_WR_Z_combined_STERR_GCON_1_chrpos.txt
head -n 1 $MetaDir/Neurodys/METAANALYSIS_Neurodys_combined_WR_Z_STERR_GCON_1.tbl > $WorkingDir/Forestplot_details.METAANALYSIS_Neurodys_combined_WR_Z_STERR_GCON_1.txt


for SNP in rs11208009
do
echo $SNP
zcat $DataDir/Alspac/CLEANED.ALSPAC_EUR_WR_Z_COMBINED_20180503.txt.gz | head -n 1 | awk '{print "Cohort", $0}' > $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/ABCD/CLEANED.ABCD_ALL_WR_Z_COMBINED_20200520.txt.gz | grep -w $SNP | awk '{print "ABCD", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/Alspac/CLEANED.ALSPAC_EUR_WR_Z_COMBINED_20180503.txt.gz | grep -w $SNP | awk '{print "Alspac", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/ASTON/CLEANED.ASTON_EUR_WR_Z_COMBINED_20200628.txt.gz | grep -w $SNP | awk '{print "ASTON", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/BATS/CLEANED.BATS_EUR_WR_Z_COMBINED_lv1grm_202006.txt.gz | grep -w $SNP | awk '{print "BATS", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/BCBL/CLEANED.BCBL_EUR_WR_Z_COMBINED_20200428.txt.gz | grep -w $SNP | awk '{print "BCBL", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/CLDRC/CLEANED.CLDRC_EUR_WR_Z_COMBINED_20200522.gz | grep -w $SNP | awk '{print "CLDRC", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/ELVS/CLEANED.ELVS_EUR_WR_Z_COMBINED_20200506.txt.gz | grep -w $SNP | awk '{print "ELVS", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/FIOLA/CLEANED.FIOLA_EUR_WR_Z_COMBINED_20200522.gz | grep -w $SNP | awk '{print "FIOLA", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/GRaD/CLEANED.GRaD_AA_WR_Z_COMBINED_20180322.gz | grep -w $SNP | awk '{print "GRaD_AA", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/GRaD/CLEANED.GRaD_HA_WR_Z_COMBINED_20180322.gz | grep -w $SNP | awk '{print "GRaD_HA", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/Iowa/CLEANED.Iowa_EUR_WR_Z_COMBINED_20180507.txt.gz | grep -w $SNP | awk '{print "Iowa", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/NTR/CLEANED.NTR_EUR_WR_Z_COMBINED_20200708.txt.gz | grep -w $SNP | awk '{print "NTR", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/PING/CLEANED.PING_ALL_WR_Z_COMBINED_20200522.txt.gz | grep -w $SNP | awk '{print "PING", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/PNC/CLEANED.PNC_EUR_WR_Z_COMBINED_20190405.txt.gz | grep -w $SNP | awk '{print "PNC", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/PNC/CLEANED.PNC2_EUR_WR_Z_COMBINED_20190405.txt.gz | grep -w $SNP | awk '{print "PNC2", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/SLIC/CLEANED.SLIC_EUR_WR_Z_COMBINED_20200522.txt.gz | grep -w $SNP | awk '{print "SLIC", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/TEDS/CLEANED.GenLang_data_full_sample_Affy.TOWREword_F.txt.gz | grep -w $SNP | awk '{print "TEDS_Affy", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/TEDS/CLEANED.GenLang_data_full_sample_OEE.TOWREword_F.txt.gz | grep -w $SNP | awk '{print "TEDS_Ilumina", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir//Toronto/CLEANED.Toronto_EUR_WR_Z_COMBINED_20200616.gz | grep -w $SNP | awk '{print "Toronto", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/UKdys/CLEANED.UKdys_EUR_WR_Z_COMBINED_20200522.txt.gz | grep -w $SNP | awk '{print "UKdys", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt
zcat $DataDir/York/CLEANED.York_EUR_WR_Z_COMBINED_20200522.txt.gz | grep -w $SNP | awk '{print "York", $0}' >> $WorkingDir/Forestplot_details.$SNP.txt

grep -w $SNP $MetaDir/Neurodys/METAANALYSIS_Neurodys_combined_WR_Z_STERR_GCON_1.tbl >> $WorkingDir/Forestplot_details.METAANALYSIS_Neurodys_combined_WR_Z_STERR_GCON_1.txt
grep -w $SNP $MetaDir/WR_Z/METAANALYSIS_WR_Z_combined_STERR_GCON_1_chrpos.txt >> $WorkingDir/Forestplot_details.METAANALYSIS_WR_Z_combined_STERR_GCON_1_chrpos.txt
done

cd $WorkingDir
# get rid of : in filenames
rename ':' '_' $WorkingDir/*.txt
# Check
wc -l $WorkingDir/Forestplot_details.*.txt



#-------------------------------------------------------------------
# in R, make the plots
#-------------------------------------------------------------------

library(forestplot)
library(stringr)

snps <- c("rs11208009")

setwd("P:/workspaces/lg-genlang/Working/Meta_analysis_METAL/WR_Z/Forestplot/")

meta <- read.table("Forestplot_details.METAANALYSIS_WR_Z_combined_STERR_GCON_1_chrpos.txt", header=TRUE)
neurodys <- read.table("Forestplot_details.METAANALYSIS_Neurodys_combined_WR_Z_STERR_GCON_1.txt", header=TRUE)


for(i in 1:length(snps)) {
	print(snps[i])
	filename <- paste("Forestplot_details", snps[i], "txt", sep=".")
	data <- read.table(filename, header=TRUE, colClasses = c("character"))
	data$BETA <- as.numeric(data$BETA)
	data$SE <- as.numeric(data$SE)
	Pvalue1 <- c("PVALUE", data$PVAL[1:11], neurodys$P.value[i], data$PVAL[12:nrow(data)], NA, meta$P.value[i])
	Cohorts <- c("COHORTS", as.vector(data$Cohort[1:11]), "Neurodys", as.vector(data$Cohort[12:nrow(data)]), NA, "Meta-analysis")
	N <- c("N", data$N[1:11], neurodys$N[i], data$N[12:nrow(data)], NA, meta$TotalSampleSize[i])
	# miminum <- data$BETA-data$SE
	if(toupper(meta$Allele1[i]) == data$EFFECT_ALLELE[1]) {
		effect <- c(NA,data$BETA[1:11], neurodys$Effect[i], data$BETA[12:nrow(data)], NA, meta$Effect[i])
		lowerbound <- c(NA,data$BETA[1:11]-data$SE[1:11], neurodys$Effect[i]-neurodys$StdErr[i],data$BETA[12:nrow(data)]-data$SE[12:nrow(data)], NA, meta$Effect[i]-meta$StdErr[i])
		upperbound <- c(NA, data$BETA[1:11]+data$SE[1:11], neurodys$Effect[i]+neurodys$StdErr[i],data$BETA[12:nrow(data)]+data$SE[12:nrow(data)], NA, meta$Effect[i]+meta$StdErr[i])
		pdf(paste("Forestplot", snps[i], "pdf", sep="."), onefile=FALSE, width=7, height=8)
		forestplot(cbind(Cohorts, N, Pvalue1), effect, lowerbound, upperbound,
			is.summary=c(TRUE, rep(FALSE,(length(data$Cohort))+1),TRUE),
			clip=c(-0.125,0.3),
			xlab="Beta for association with word reading",
			title=snps[i],
			vertices = TRUE,
			txt_gp = fpTxtGp(label = gpar(fontfamily = "", cex=1),
                     ticks = gpar(fontfamily = "", cex=0.7),
                     xlab  = gpar(fontfamily = "", cex = 1)))
		dev.off()
		png(paste("Forestplot", snps[i], "png", sep="."), width=7, height=8, units="in", res=900)
		forestplot(cbind(Cohorts, N, Pvalue1), effect, lowerbound, upperbound,
			is.summary=c(TRUE, rep(FALSE,(length(data$Cohort))+1),TRUE),
			clip=c(-0.125,0.3),
			xlab="Beta for association with word reading",
			title=snps[i],
			vertices = TRUE,
			txt_gp = fpTxtGp(label = gpar(fontfamily = "", cex=1),
                     ticks = gpar(fontfamily = "", cex=0.7),
                     xlab  = gpar(fontfamily = "", cex = 1)))
		dev.off()
		}
	if(toupper(meta$Allele2[i]) == data$EFFECT_ALLELE[1]) {
		pdf(filename, onefile=FALSE)
			forestplot(cbind(Cohorts, N, Pvalue1), c(NA,data$BETA, -neurodys$Effect[i]), NA, -meta$Effect[i]), c(NA,data$BETA-data$SE, -neurodys$Effect[i]-neurodys$StdErr[i]), NA, -meta$Effect[i]-meta$StdErr[i]), c(NA, data$BETA+data$SE, -neurodys$Effect[i]+neurodys$StdErr[i], NA, -meta$Effect[i]+meta$StdErr[i]),
			is.summary=c(TRUE, rep(FALSE,length(data$Cohort)),TRUE),
			# clip=c(-0.15,0.2),
			xlab="Beta for association with word reading",
			title=snps[i],
			vertices = TRUE,
			txt_gp = fpTxtGp(label = gpar(fontfamily = "", cex=1),
                     ticks = gpar(fontfamily = "", cex=0.7),
                     xlab  = gpar(fontfamily = "", cex = 1)))
		dev.off()
		}
	}

	
