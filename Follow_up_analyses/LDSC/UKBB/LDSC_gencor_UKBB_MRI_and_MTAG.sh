 #\!/bin/sh
#$ -N LDSC_GenLang_UKB_IDPs
#$ -o /data/workspaces/lag/workspaces/lg-genlang/Working/UKBB/log
#$ -e /data/workspaces/lag/workspaces/lg-genlang/Working/UKBB/log
#$ -cwd
#$ -q single.q
#$ -S /bin/bash
#$ -M barbara.molz@mpi.nl
#$ -m beas

DATE=`date +"%Y%m%d"`
#-------------------------------------------------------------------------------------
#set up our files: the general input path and the outputfolder
#munged sumstats
ukb_sumstats_workspace=/data/workspaces/lag/workspaces/lg-genlang/Working/UKBB/munged_IDPs
ukb_sumstats_cluster=/data/clusterfs/lag/users/barmol/genlang/munged
genlang_sumstats=/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_after_metaanalysis/LDSC/MTAG/MTAG_WR_NREAD_SP_PA_trait_1.sumstats.gz
#output gen cor
out_path_gencor_cluster=/data/clusterfs/lag/users/barmol/genlang/gencor
out_path_gencor_workspace=/data/workspaces/lag/workspaces/lg-genlang/Working/UKBB/gencor
# details for LDSC
resource_dir=/data/workspaces/lag/shared_spaces/Resource_DB
ldscores_dir=${resource_dir}/LDscores

 #---------------------------------------------------------------------------------------------------
#copy ukb IDPs to cluster first
cd $ukb_sumstats_workspace
cp *gz $ukb_sumstats_cluster

#set up stuff for gen cor
#---------------------------------------------------------------------------------------------------
# run LDSC genetic correlation between UKB IDps and Genlang
for ukbIDP in ${ukb_sumstats_cluster}/*gz;do
	 filename=${ukbIDP##*/}
	 phenotype=${filename%.s*gz} 
	 echo 'Run gen corr between genlang and UKDP: ' ${phenotype}
	 /usr/local/apps/python-2.7.11/bin/python /usr/local/apps/ldsc/ldsc.py \
	--rg $genlang_sumstats,$ukbIDP \
	--ref-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
	--w-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
	--out ${out_path_gencor_cluster}/gencor_MTAG_WR_NREAD_SP_PA_trait_1_${phenotype}
done

echo 'LDSC gen cor finished, copying files to workspace'
cd $out_path_gencor_cluster
cp * $out_path_gencor_workspace
