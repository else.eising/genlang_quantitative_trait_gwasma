#\!/bin/sh
#$ -N LDSC_GenLang_UKB_IDPs
#$ -o /data/workspaces/lag/workspaces/lg-genlang/Working/UKBB/log
#$ -e /data/workspaces/lag/workspaces/lg-genlang/Working/UKBB/log
#$ -cwd
#$ -q single.q
#$ -S /bin/bash
#$ -M barbara.molz@mpi.nl
#$ -m beas

DATE=`date +"%Y%m%d"`
#---------------------------------------------------------------------------------------------------
#set up our files: the general input path and the outputfolder
#and munged sumstats
#---------------------------------------------------------------------------------------------------
ukb_sumstats_cluster=/data/clusterfs/lag/users/barmol/genlang/munged
#output gen cor
out_path_gencor_cluster=/data/clusterfs/lag/users/barmol/genlang/gencor
out_path_gencor_workspace=/data/workspaces/lag/workspaces/lg-genlang/Working/UKBB/gencor_imaging
# details for LDSC
resource_dir=/data/workspaces/lag/shared_spaces/Resource_DB
ldscores_dir=${resource_dir}/LDscores

#---------------------------------------------------------------------------------------------------
#set up stuff for gen cor
#----------------------------------------------------------------------------------------------------
# run LDSC genetic correlation between UKB IDps and Genlang
for ukbIDP_1 in ${ukb_sumstats_cluster}/*gz;do
	 filename_1=${ukbIDP_1##*/}
	 phenotype_1=${filename_1%.s*gz}
	for ukbIDP_2 in ${ukb_sumstats_cluster}/*gz;do
		filename_2=${ukbIDP_2##*/}
		phenotype_2=${filename_2%.s*gz} 
		echo 'Run gen corr between genlang and UKDP: ' ${phenotype}
		/usr/local/apps/python-2.7.11/bin/python /usr/local/apps/ldsc/ldsc.py \
		--rg $ukbIDP_1,$ukbIDP_2 \
		--ref-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
		--w-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
		--out ${out_path_gencor_cluster}/gencor_${phenotype_1}_${phenotype_2}
	done
done

#---------------------------------------------------------------------------------------------------
# copy files back to workspace
#----------------------------------------------------------------------------------------------------
echo 'LDSC gen cor finished, copying files to workspace'
cd $out_path_gencor_cluster
cp * $out_path_gencor_workspace

 #---------------------------------------------------------------------------------------------------
 # extract gen cor resuls 
#----------------------------------------------------------------------------------------------------
#little wrapper to extract the summary values from the LDSC log after running gen cor
echo "trait rg se z p h2_obs h2_obs_se h2_int h2_int_se gcov_int gcov_int_se"> $out_path_gencor_workspace/gencor_ukbBig40_genlangtraits.txt
for i in $out_path_gencor_workspace/*log; do
  res=$(cat $i | grep '^/d.*sumstat' | cut -d" " -f4-)
  pheno=$(basename "$i" | cut -d. -f1)
  echo $pheno $res >> $out_path_gencor_workspace/gencor_ukbBig40_genlangtraits.txt
done 