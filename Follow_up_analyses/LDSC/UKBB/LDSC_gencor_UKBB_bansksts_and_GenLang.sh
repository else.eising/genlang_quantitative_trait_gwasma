#\!/bin/sh
#$ -N LDSC_GenLang_UKB_IDPs
#$ -o /data/workspaces/lag/workspaces/lg-genlang/Working/UKBB/log
#$ -e /data/workspaces/lag/workspaces/lg-genlang/Working/UKBB/log
#$ -cwd
#$ -q single.q
#$ -S /bin/bash
#$ -M barbara.molz@mpi.nl
#$ -m beas

DATE=`date +"%Y%m%d"`
#---------------------------------------------------------------------------------------------------
#set up our files: the general input path and the outputfolder
#and munged sumstats
#---------------------------------------------------------------------------------------------------
ukb_sumstats_workspace=/data/workspaces/lag/workspaces/lg-genlang/Working/UKBB/munged_IDPs/munged_aparc-Desikan_lh_area_bankssts.sumstats.gz
genlang_sumstats=/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_after_metaanalysis/LDSC/RT_combined
#output gen cor
out_path_gencor_workspace=/data/workspaces/lag/workspaces/lg-genlang/Working/UKBB/gencor_RT_combined
# details for LDSC
resource_dir=/data/workspaces/lag/shared_spaces/Resource_DB
ldscores_dir=${resource_dir}/LDscores

#---------------------------------------------------------------------------------------------------
#copy files to clusterfs
#---------------------------------------------------------------------------------------------------
# echo 'copying files to clusterfs'
# cd $ukb_sumstats_workspace
# cp *gz $ukb_sumstats_cluster

#---------------------------------------------------------------------------------------------------
#set up stuff for gen cor
#----------------------------------------------------------------------------------------------------
# run LDSC genetic correlation between UKB IDps and Genlang
for genlang in ${genlang_sumstats}/*gz;do
	 filename_1=${genlang##*/}
	 phenotype_1=${filename_1%.s*gz}
	 echo 'Run gen corr between genlang and bankssts: '${phenotype_1} 
	 /usr/local/apps/python-2.7.11/bin/python /usr/local/apps/ldsc/ldsc.py \
	--rg $genlang,$ukb_sumstats_workspace \
	--ref-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
	--w-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
	--out ${out_path_gencor_workspace}/gencor_${phenotype_1}_aparc-Desikan_lh_area_bankssts
done



 #---------------------------------------------------------------------------------------------------
 # extract gen cor resuls 
#----------------------------------------------------------------------------------------------------
#little wrapper to extract the summary values from the LDSC log after running gen cor
echo "trait rg se z p h2_obs h2_obs_se h2_int h2_int_se gcov_int gcov_int_se"> ${out_path_gencor_workspace}/gencor_RT_combined_aparc-Desikan_lh_area_bankssts.txt
for i in ${out_path_gencor_workspace}/*log; do
  res=$(cat $i | grep '^/d.*sumstat' | cut -d" " -f4-)
  pheno=$(basename "$i" | cut -d. -f1)
  echo $pheno $res >> ${out_path_gencor_workspace}/gencor_RT_combined_aparc-Desikan_lh_area_bankssts.txt
done 
#