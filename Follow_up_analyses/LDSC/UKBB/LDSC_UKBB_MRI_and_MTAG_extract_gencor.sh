#!/bin/bash
#little wrapper to extract the summary values from the LDSC log after running gen corl 
gencor_path=/data/workspaces/lag/workspaces/lg-genlang/Working/UKBB/gencor
echo "trait rg se z p h2_obs h2_obs_se h2_int h2_int_se gcov_int gcov_int_se"> ${gencor_path}/gencor_MTAG_WR_NREAD_SP_PA_trait_1_ukbBig40.txt
for i in ${gencor_path}/*log; do
  res=$(cat $i | grep '^/d.*sumstat' | cut -d" " -f4-)
  pheno=$(basename "$i" | cut -d. -f1)
  echo $pheno $res >> ${gencor_path}/gencor_MTAG_WR_NREAD_SP_PA_trait_1_ukbBig40.txt
done 