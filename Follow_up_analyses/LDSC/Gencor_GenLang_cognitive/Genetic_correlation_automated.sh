#!/bin/bash
#LDSC constrained genetic correlation
#$1 - file(s) 1 prefix munged sumstats
#$2 - file(s) 2 prefix munges sumstats
#$3 - directory

# to run this script, use the script Genetic_correlation_automated_submit_to_grid.sh located in the same folder

if [ ! -d "$3/LDSC_output/" ]; then
mkdir $3/LDSC_output/
fi


for var in $1 
do
echo $var
v=$(echo $var| sed 's|^METAANALYSIS_||g' | sed 's/_1_rsID//g' | sed 's/_EUR//g' | sed 's/_1//g' | sed 's/_combined_STERR_GCOFF//g' | sed 's/_STERR_GCOFF//g' | sed 's/.tbl//g' | sed 's/_WR_NREAD_SP_PA_trait_1//g')
echo $v

for seq in $2
do 
echo $seq
s=$(echo $seq| sed 's|^METAANALYSIS_||g' | sed 's/_1_rsID//g' | sed 's/_EUR//g' | sed 's/_1//g' | sed 's/_combined_STERR_GCOFF//g' | sed 's/_STERR_GCOFF//g' | sed 's/.tbl//g' | sed 's/_GWAS_excl23andMe_20201104_buildGRCh37//g')
echo $s

/data/workspaces/lag/shared_spaces/Resource_DB/ldsc/ldsc.py \
--rg $3/$var.sumstats.gz,$3/$seq.sumstats.gz \
--ref-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
--w-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
--no-check-alleles \
--out $3/LDSC_output/temp


cp $3/LDSC_output/temp.log $3/LDSC_output/$v.$s.ldsc.log
rm $3/LDSC_output/temp.log
	
echo $v > $3/LDSC_output/tmp1
echo $s > $3/LDSC_output/tmp2

grep gencov: $3/LDSC_output/$v.$s.ldsc.log > $3/LDSC_output/tmp3

grep -A1 'p1' $3/LDSC_output/$v.$s.ldsc.log | grep -v "p1" | awk '{print $3, $4, $5, $6, $7, $8, $9, $10, $11, $12}' > $3/LDSC_output/tmp4

echo "File1	File2	Total Observed scale gencov: Gencov Gencov_SD	rg se z p h2_obs h2_obs_se h2_int h2_int_se gcov_int gcov_int_se" > $3/LDSC_output/$v.$s.ldsc.out
paste $3/LDSC_output/tmp1 $3/LDSC_output/tmp2 $3/LDSC_output/tmp3 $3/LDSC_output/tmp4 >> $3/LDSC_output/$v.$s.ldsc.out
rm $3/LDSC_output/tmp1 $3/LDSC_output/tmp2 $3/LDSC_output/tmp3 $3/LDSC_output/tmp4

done
done