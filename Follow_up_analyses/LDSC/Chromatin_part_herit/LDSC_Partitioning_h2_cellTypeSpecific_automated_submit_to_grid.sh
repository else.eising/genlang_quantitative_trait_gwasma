#################################################################################################################
# By Else Eising
# Script for partitioning heritability with LDSC
#################################################################################################################

# The LDSC_Partitioning_h2_cellTypeSpecific_automated.sh script should be submitted with a projectname, filename, and name for the lcdts filename
# Project name: e.g. MTAG, RT_combined (folder names in $WorkingDir)
# Name of the summary statistics file that is already prepped with the munge summary statistics function of LDSC, without ".sumstats.gz"
# Name of the ldcts file that describes what datasets to use for the analysis, without ".ldcts". These can be found in $LDScoreDir. E.g. Cahoy

cd /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_after_metaanalysis/LDSC/

qsub -v PROJECT=MTAG,FILE=MTAG_WR_NREAD_SP_PA_trait_1,LDCTSNAME=Multi_tissue_chromatin LDSC_Partitioning_h2_cellTypeSpecific_automated.sh 
