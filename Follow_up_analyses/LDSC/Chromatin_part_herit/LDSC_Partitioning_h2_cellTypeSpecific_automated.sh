#\!/bin/sh
#$ -N LDSC_partitioning_h2
#$ -o /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_after_metaanalysis/LDSC/logs
#$ -e /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_after_metaanalysis/LDSC/logs
#$ -cwd
#$ -q single15.q
#$ -S /bin/bash
#$ -M else.eising@mpi.nl
#$ -m beas


#################################################################################################################
# By Else Eising
# Script for partitioning heritability with LDSC
#################################################################################################################

# Define paths:
LDSCDir="/data/workspaces/lag/shared_spaces/Resource_DB/ldsc/"
LDScoreDir="/data/workspaces/lag/shared_spaces/Resource_DB/LDscores/Phase3/"
WorkingDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_after_metaanalysis/LDSC/"
MTAGDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_MTAG/"

# Define arguments
# This script should be submitted with a projectname, filename, and name for the lcdts filename
# Project name: e.g. MTAG, RT_combined (folder names in $WorkingDir)
# Name of the summary statistics file that is already prepped with the munge summary statistics function of LDSC, without ".sumstats.gz"
# Name of the ldcts file that describes what datasets to use for the analysis, without ".ldcts". These can be found in $LDScoreDir. E.g. Cahoy

#################################################################################################################
## Cell-type specific
#################################################################################################################

# Only taking the RT phentoypes, so that processing of phenotypes matches
# Including only results based on Europeans, so I can use 1KG LD information
# Use results that were not GC corrected!

cd $LDScoreDir

$LDSCDir/ldsc.py \
    --h2-cts $WorkingDir/$PROJECT/$FILE.sumstats.gz\
    --ref-ld-chr $LDScoreDir/1000G_EUR_Phase3_baseline/baseline.\
    --w-ld-chr $LDScoreDir/1000G_Phase3_weights_hm3_no_MHC/weights.hm3_noMHC.\
    --out $WorkingDir/$PROJECT/Partitioning_h2/$FILE.${LDCTSNAME} \
    --print-coefficients \
    --ref-ld-chr-cts $LDScoreDir/$LDCTSNAME.ldcts


