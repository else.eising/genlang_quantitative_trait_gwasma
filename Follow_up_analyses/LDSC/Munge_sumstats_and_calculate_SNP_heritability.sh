#################################################################################################################
# By Else Eising
# Script to run LDSC: munge sumstats and calculate SNP heritability
#################################################################################################################

# Define paths:
LDSCDir="/data/workspaces/lag/shared_spaces/Resource_DB/ldsc/"
LDScoreDir="/data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/"
DataDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/"
MTAGDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_MTAG/"
ResultsDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_after_metaanalysis/LDSC/"


#################################################################################################################
## Step 1: Reformatting Summary Statistics
#################################################################################################################

# Only taking the RT phentoypes, so that processing of phenotypes matches
# Including only results based on Europeans, so I can use 1KG LD information
# Use results that were not GC corrected!

#----
## RT combined
#----
echo "WR_RT/METAANALYSIS_WR_RT_EUR_combined_STERR_GCOFF_1.tbl
NREAD_RT/METAANALYSIS_NREAD_RT_EUR_combined_STERR_GCOFF_1.tbl
SP_RT/METAANALYSIS_SP_RT_EUR_combined_STERR_GCOFF_1.tbl
PA_RT/METAANALYSIS_PA_RT_EUR_combined_STERR_GCOFF_1.tbl
PIQ_RT/METAANALYSIS_PIQ_RT_combined_STERR_GCOFF_1.tbl" > $ResultsDir/RT_combined/Files_to_be_included.txt

while read file; do
outfile=$(echo $file | sed 's|^[A-Z]*_RT/||g' | sed 's|.tbl||g' )
echo $outfile
	$LDSCDir/munge_sumstats.py \
	--sumstats $DataDir/$file \
	--snp MarkerName \
	--a1 Allele1 \
	--a2 Allele2 \
	--N-col TotalSampleSize \
	--p PVAL \
	--frq Freq1 \
	--signed-sumstats Effect,0 \
	--out $ResultsDir/RT_combined/$outfile \
	--merge-alleles $LDScoreDir/w_hm3.snplist
done < $ResultsDir/RT_combined/Files_to_be_included.txt

# Calculate heritability
while read file; do
outfile=$(echo $file | sed 's|^[A-Z]*_RT/||g' | sed 's|.tbl||g' )
echo ${outfile}
	$LDSCDir/ldsc.py \
	--h2 $ResultsDir/RT_combined/${outfile}.sumstats.gz \
	--ref-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
	--w-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
	--out $ResultsDir/RT_combined/${outfile}.heritability 
done < $ResultsDir/RT_combined/Files_to_be_included.txt

# separately for NREP_RT (random effect meta-analysis)
$LDSCDir/munge_sumstats.py \
	--sumstats $DataDir/NREP_RT/METAANALYSIS_RANDOM_NREP_RT_EUR_combined_STERR_GCOFF_1.tbl \
	--snp MarkerName \
	--a1 Allele1 \
	--a2 Allele2 \
	--N-col TotalSampleSize \
	--p Pvalue \
	--frq Freq1 \
	--signed-sumstats Effect,0 \
	--out $ResultsDir/RT_combined/METAANALYSIS_NREP_RT_EUR_combined_STERR_GCOFF_1 \
	--merge-alleles $LDScoreDir/w_hm3.snplist
	
	$LDSCDir/ldsc.py \
	--h2 $ResultsDir/RT_combined/METAANALYSIS_NREP_RT_EUR_combined_STERR_GCOFF_1.sumstats.gz \
	--ref-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
	--w-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
	--out $ResultsDir/RT_combined/METAANALYSIS_NREP_RT_EUR_combined_STERR_GCOFF_1.heritability 

#----
## Repeat this for the males- and females-only subsets
#----
echo "WR_RT_MALE/METAANALYSIS_WR_RT_MALE_EUR_STERR_GCOFF_1.tbl
NREAD_RT_MALE/METAANALYSIS_NREAD_RT_MALE_EUR_STERR_GCOFF_1.tbl
NREP_RT_MALE/METAANALYSIS_NREP_RT_MALE_EUR_STERR_GCOFF_1.tbl
SP_RT_MALE/METAANALYSIS_SP_RT_MALE_EUR_STERR_GCOFF_1.tbl
PA_RT_MALE/METAANALYSIS_PA_RT_MALE_EUR_STERR_GCOFF_1.tbl
PIQ_RT_MALE/METAANALYSIS_PIQ_RT_MALE_STERR_GCOFF_1.tbl
WR_RT_FEMALE/METAANALYSIS_WR_RT_FEMALE_EUR_STERR_GCOFF_1.tbl
NREAD_RT_FEMALE/METAANALYSIS_NREAD_RT_FEMALE_EUR_STERR_GCOFF_1.tbl
NREP_RT_FEMALE/METAANALYSIS_NREP_RT_FEMALE_EUR_STERR_GCOFF_1.tbl
SP_RT_FEMALE/METAANALYSIS_SP_RT_FEMALE_EUR_STERR_GCOFF_1.tbl
PA_RT_FEMALE/METAANALYSIS_PA_RT_FEMALE_EUR_STERR_GCOFF_1.tbl
PIQ_RT_FEMALE/METAANALYSIS_PIQ_RT_FEMALE_STERR_GCOFF_1.tbl" > $ResultsDir/RT_combined_males_and_females/Files_to_be_included.txt

while read file; do
outfile=$(echo $file | sed 's|^[A-Z]*_RT_MALE/||g' | sed 's|^[A-Z]*_RT_FEMALE/||g' | sed 's|.tbl||g' )
echo $outfile
if [ ! -f $ResultsDir/RT_combined_males_and_females/$outfile'.sumstats.gz' ];
   then
	$LDSCDir/munge_sumstats.py \
	--sumstats $DataDir/$file \
	--snp MarkerName \
	--a1 Allele1 \
	--a2 Allele2 \
	--N-col TotalSampleSize \
	--p PVAL \
	--frq Freq1 \
	--signed-sumstats Effect,0 \
	--out $ResultsDir/RT_combined_males_and_females/$outfile \
	--merge-alleles $LDScoreDir/w_hm3.snplist
fi
done < $ResultsDir/RT_combined_males_and_females/Files_to_be_included.txt

# Calculate heritability
while read file; do
outfile=$(echo $file | sed 's|^[A-Z]*_RT_MALE/||g' | sed 's|^[A-Z]*_RT_FEMALE/||g' | sed 's|.tbl||g' )
echo ${outfile}
if [ ! -f $ResultsDir/RT_combined_males_and_females/$outfile'.heritability.log' ];
   then
	$LDSCDir/ldsc.py \
	--h2 $ResultsDir/RT_combined_males_and_females/${outfile}.sumstats.gz \
	--ref-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
	--w-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
	--out $ResultsDir/RT_combined_males_and_females/${outfile}.heritability 
fi
done < $ResultsDir/RT_combined_males_and_females/Files_to_be_included.txt


#----
## Compare the results of the different reading tasks
#----
echo "WR_RT/METAANALYSIS_WR_RT_EUR_combined_STERR_GCOFF_nonTOWRE_1.tbl
WR_RT/METAANALYSIS_WR_RT_EUR_combined_STERR_GCOFF_TOWRE_1.tbl
WR_RT/METAANALYSIS_WR_RT_EUR_combined_STERR_GCOFF_nonTimed_1.tbl
WR_RT/METAANALYSIS_WR_RT_EUR_combined_STERR_GCOFF_Timed_1.tbl" > $ResultsDir/RT_combined_WR_tasks/Files_to_be_included.txt

while read file; do
outfile=$(echo $file | sed 's|^[A-Z]*_RT/||g' | sed 's|.tbl||g' )
echo $outfile
if [ ! -f $ResultsDir/RT_combined_WR_tasks/$outfile'.sumstats.gz' ];
   then
	$LDSCDir/munge_sumstats.py \
	--sumstats $DataDir/$file \
	--snp MarkerName \
	--a1 Allele1 \
	--a2 Allele2 \
	--N-col TotalSampleSize \
	--p PVAL \
	--frq Freq1 \
	--signed-sumstats Effect,0 \
	--out $ResultsDir/RT_combined_WR_tasks/$outfile \
	--merge-alleles $LDScoreDir/w_hm3.snplist
	fi
done < $ResultsDir/RT_combined_WR_tasks/Files_to_be_included.txt

# Calculate heritability
while read file; do
outfile=$(echo $file | sed 's|^[A-Z]*_RT/||g' | sed 's|.tbl||g' )
echo ${outfile}
if [ ! -f $ResultsDir/RT_combined_WR_tasks/$outfile'.heritability.log' ];
   then
	$LDSCDir/ldsc.py \
	--h2 $ResultsDir/RT_combined_WR_tasks/${outfile}.sumstats.gz \
	--ref-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
	--w-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
	--out $ResultsDir/RT_combined_WR_tasks/${outfile}.heritability 
	fi
done < $ResultsDir/RT_combined_WR_tasks/Files_to_be_included.txt

#----
## Compare the results of the different nonword reading tasks
#----
echo "NREAD_RT/METAANALYSIS_NREAD_RT_EUR_combined_STERR_GCOFF_TOWRE_1.tbl
NREAD_RT/METAANALYSIS_NREAD_RT_EUR_combined_STERR_GCOFF_nonTOWRE_1.tbl
NREAD_RT/METAANALYSIS_NREAD_RT_EUR_combined_STERR_GCOFF_Timed_1.tbl
NREAD_RT/METAANALYSIS_NREAD_RT_EUR_combined_STERR_GCOFF_nonTimed_1.tbl" > $ResultsDir/RT_combined_NREAD_tasks/Files_to_be_included.txt

while read file; do
outfile=$(echo $file | sed 's|^[A-Z]*_RT/||g' | sed 's|.tbl||g' )
echo $outfile
if [ ! -f $ResultsDir/RT_combined_NREAD_tasks/$outfile'.sumstats.gz' ];
then
$LDSCDir/munge_sumstats.py \
--sumstats $DataDir/$file \
--snp MarkerName \
--a1 Allele1 \
--a2 Allele2 \
--N-col TotalSampleSize \
--p PVAL \
--frq Freq1 \
--signed-sumstats Effect,0 \
--out $ResultsDir/RT_combined_NREAD_tasks/$outfile \
--merge-alleles $LDScoreDir/w_hm3.snplist
fi
done < $ResultsDir/RT_combined_NREAD_tasks/Files_to_be_included.txt

# Calculate heritability
while read file; do
outfile=$(echo $file | sed 's|^[A-Z]*_RT/||g' | sed 's|.tbl||g' )
echo ${outfile}
if [ ! -f $ResultsDir/RT_combined_NREAD_tasks/$outfile'.heritability.log' ];
   then
	$LDSCDir/ldsc.py \
	--h2 $ResultsDir/RT_combined_NREAD_tasks/${outfile}.sumstats.gz \
	--ref-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
	--w-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
	--out $ResultsDir/RT_combined_NREAD_tasks/${outfile}.heritability 
	fi
done < $ResultsDir/RT_combined_NREAD_tasks/Files_to_be_included.txt

#----
## Compare the results of the word reading results of the 'old' and 'young' subsets
#----
echo "WR_RT/METAANALYSIS_WR_RT_EUR_combined_STERR_GCOFF_Old_1.tbl
WR_RT/METAANALYSIS_WR_RT_EUR_combined_STERR_GCOFF_Young_1.tbl" > $ResultsDir/RT_combined_WR_age_stratified/Files_to_be_included.txt

while read file; do
outfile=$(echo $file | sed 's|^[A-Z]*_RT/||g' | sed 's|.tbl||g' )
echo $outfile
if [ ! -f $ResultsDir/RT_combined_WR_age_stratified/$outfile'.sumstats.gz' ];
then
$LDSCDir/munge_sumstats.py \
--sumstats $DataDir/$file \
--snp MarkerName \
--a1 Allele1 \
--a2 Allele2 \
--N-col TotalSampleSize \
--p PVAL \
--frq Freq1 \
--signed-sumstats Effect,0 \
--out $ResultsDir/RT_combined_WR_age_stratified/$outfile \
--merge-alleles $LDScoreDir/w_hm3.snplist
fi
done < $ResultsDir/RT_combined_WR_age_stratified/Files_to_be_included.txt

# Calculate heritability
while read file; do
outfile=$(echo $file | sed 's|^[A-Z]*_RT/||g' | sed 's|.tbl||g' )
echo ${outfile}
if [ ! -f $ResultsDir/RT_combined_WR_age_stratified/$outfile'.heritability.log' ];
   then
	$LDSCDir/ldsc.py \
	--h2 $ResultsDir/RT_combined_WR_age_stratified/${outfile}.sumstats.gz \
	--ref-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
	--w-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
	--out $ResultsDir/RT_combined_WR_age_stratified/${outfile}.heritability 
	fi
done < $ResultsDir/RT_combined_WR_age_stratified/Files_to_be_included.txt

#----
## Compare the results of the nonword reading results of the 'old' and 'young' subsets
#----
echo "NREAD_RT/METAANALYSIS_NREAD_RT_EUR_combined_STERR_GCOFF_Old_1.tbl
NREAD_RT/METAANALYSIS_NREAD_RT_EUR_combined_STERR_GCOFF_Young_1.tbl" > $ResultsDir/RT_combined_NREAD_age_stratified/Files_to_be_included.txt

while read file; do
outfile=$(echo $file | sed 's|^[A-Z]*_RT/||g' | sed 's|.tbl||g' )
echo $outfile
$LDSCDir/munge_sumstats.py \
--sumstats $DataDir/$file \
--snp MarkerName \
--a1 Allele1 \
--a2 Allele2 \
--N-col TotalSampleSize \
--p PVAL \
--frq Freq1 \
--signed-sumstats Effect,0 \
--out $ResultsDir/RT_combined_NREAD_age_stratified/$outfile \
--merge-alleles $LDScoreDir/w_hm3.snplist
done < $ResultsDir/RT_combined_NREAD_age_stratified/Files_to_be_included.txt

# Calculate heritability
while read file; do
outfile=$(echo $file | sed 's|^[A-Z]*_RT/||g' | sed 's|.tbl||g' )
echo ${outfile}
	$LDSCDir/ldsc.py \
	--h2 $ResultsDir/RT_combined_NREAD_age_stratified/${outfile}.sumstats.gz \
	--ref-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
	--w-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
	--out $ResultsDir/RT_combined_NREAD_age_stratified/${outfile}.heritability 
done < $ResultsDir/RT_combined_NREAD_age_stratified/Files_to_be_included.txt

#----
## Munge the external sumstats: Danish schoolgrade PCs
#----
echo "Schoolgrades_E1_iPSYCH_sumstats.gz
Schoolgrades_E2_iPSYCH_sumstats.gz
Schoolgrades_E3_iPSYCH_sumstats.gz
Schoolgrades_E4_iPSYCH_sumstats.gz" > $ResultsDir/RT_combined_external_sumstats/Files_to_be_included.txt

while read file; do
outfile=$(echo $file | sed 's|_sumstats.gz||g' )
echo $outfile
if [ ! -f $ResultsDir/RT_combined_external_sumstats/$outfile'.sumstats.gz' ];
   then
	$LDSCDir/munge_sumstats.py \
	--sumstats /data/workspaces/lag/workspaces/lg-genlang/Working/External_sumstats/iPSYCH/$file \
	--snp SNP \
	--a1 A1 \
	--a2 A2 \
	--N-col N \
	--p P \
	--frq Freq1 \
	--signed-sumstats BETA,0 \
	--out $ResultsDir/RT_combined_external_sumstats/$outfile \
	--merge-alleles $LDScoreDir/w_hm3.snplist
	fi
done < $ResultsDir/RT_combined_external_sumstats/Files_to_be_included.txt

#----
## Munge the external sumstats of the GWAS by subtraction: nonCog
#----

$LDSCDir/munge_sumstats.py \
--sumstats /data/workspaces/lag/workspaces/lg-genlang/Working/External_sumstats/nonCog_Cog/NonCog_GWAS_excl23andMe_20201104_buildGRCh37.tsv.gz \
--snp variant_id \
--a1 effect_allele \
--a2 other_allele \
--N 510795 \
--p p_value \
--frq MAF \
--signed-sumstats est,0 \
--out $ResultsDir/RT_combined_external_sumstats/NonCog_GWAS_excl23andMe_20201104_buildGRCh37 \
--merge-alleles $LDScoreDir/w_hm3.snplist

#----
## Munge the external sumstats of Lee 2018: EA and CP
#----
$LDSCDir/munge_sumstats.py \
--sumstats /data/workspaces/lag/workspaces/lg-genlang/Working/External_sumstats/Lee_2018_EA/GWAS_EA_excl23andMe.txt \
--snp MarkerName \
--a1 A1 \
--a2 A2 \
--N 766345 \
--p Pval \
--signed-sumstats Beta,0 \
--out $ResultsDir/RT_combined_external_sumstats/GWAS_EA_excl23andMe \
--merge-alleles $LDScoreDir/w_hm3.snplist

$LDSCDir/munge_sumstats.py \
--sumstats /data/workspaces/lag/workspaces/lg-genlang/Working/External_sumstats/Lee_2018_EA/GWAS_CP_all.txt \
--snp MarkerName \
--a1 A1 \
--a2 A2 \
--N 257828 \
--p Pval \
--signed-sumstats Beta,0 \
--out $ResultsDir/RT_combined_external_sumstats/GWAS_CP_all \
--merge-alleles $LDScoreDir/w_hm3.snplist

#----
## munge the sumstats of the multivariate reading results of MTAG, and calculate SNP heritability
#----

# get GWAS equivalent N from MTAG log! Use that as N

$LDSCDir/munge_sumstats.py \
--sumstats $MTAGDir/MTAG_WR_NREAD_SP_PA_trait_1.txt \
--snp SNP \
--a1 A1 \
--a2 A2 \
--N 41783 \
--p mtag_pval \
--frq FRQ \
--signed-sumstats mtag_beta,0 \
--out $ResultsDir/MTAG/MTAG_WR_NREAD_SP_PA_trait_1 \
--merge-alleles $LDScoreDir/w_hm3.snplist

# Calculate heritability
 $LDSCDir/ldsc.py \
 --h2 $ResultsDir/MTAG/MTAG_WR_NREAD_SP_PA_trait_1.sumstats.gz \
 --ref-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
 --w-ld-chr /data/workspaces/lag/shared_spaces/Resource_DB/LDscores/eur_w_ld_chr/ \
 --out $ResultsDir/MTAG/MTAG_WR_NREAD_SP_PA_trait_1.heritability 


#################################################################################################################
## Step 2: Check for warnings
#################################################################################################################

# if the mean chi2 is not above 1.02, there is not much polygenic signal to work with. if mean chi2 is below 1, ldsc will not work

cd $ResultsDir/RT_combined
grep 'WARNING' *log
grep 'ERROR' *log
grep 'chi^2' *log 
ls | grep 'sumstats.gz'
ls | grep 'sumstats.gz' | wc -l


cd $ResultsDir/RT_combined_WR_tasks
grep 'WARNING' *log
grep 'ERROR' *log
grep 'chi^2' *log 
ls | grep 'sumstats.gz'
ls | grep 'sumstats.gz' | wc -l

cd $ResultsDir/RT_combined_NREAD_tasks
grep 'WARNING' *log
grep 'ERROR' *log
grep 'chi^2' *log 
ls | grep 'sumstats.gz'
ls | grep 'sumstats.gz' | wc -l

cd $ResultsDir/RT_combined_WR_age_stratified
grep 'WARNING' *log
grep 'ERROR' *log
grep 'chi^2' *log 
ls | grep 'sumstats.gz'
ls | grep 'sumstats.gz' | wc -l

cd $ResultsDir/RT_combined_NREAD_age_stratified
grep 'WARNING' *log
grep 'ERROR' *log
grep 'chi^2' *log 
ls | grep 'sumstats.gz'
ls | grep 'sumstats.gz' | wc -l

