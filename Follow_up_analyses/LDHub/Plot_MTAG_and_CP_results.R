#----------------------------------------------------------------------------------------------------------
# script for the plotting of LDhub results
# By Else Eising, based on script from Kate Doust
# The input file is formatted in Excel to have the appropriate columns and to select the traits to plot
#----------------------------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------------------------
# Load libraries and set directories
#----------------------------------------------------------------------------------------------------------

library(corrplot)
library(plyr)
library(ggplot2)
library(tidyverse)
library(grid)
library(viridis)


DataDir <- "P:/workspaces/lg-genlang/Working/Genotypes_QC_after_metaanalysis/LDHub/"

#----------------------------------------------------------------------------------------------------------
# Load data
#----------------------------------------------------------------------------------------------------------

LDhub <- read.csv(paste(DataDir, "MTAG_and_CP_LDhub_UKBB_education_cognitive.Significant_only_for_plot_v2.csv", sep=""), stringsAsFactors=FALSE, header=TRUE)

# add column that specifies whether MTAG or CP has significant correlation
LDhub$Significance_MTAG <- "significant"
LDhub$Significance_CP <- "significant"
LDhub$Significance_MTAG[which(LDhub$p_MTAG > (0.05/(535*2)))] <- "not significant"
LDhub$Significance_CP[which(LDhub$p_CP > (0.05/(535*2)))] <- "not significant"

#----------------------------------------------------------------------------------------------------------
# Plot data
#----------------------------------------------------------------------------------------------------------

LDhub$Short_name <- factor(LDhub$Y_axis_labels, levels=unique(LDhub$Y_axis_labels)[order(unique(LDhub$Order), decreasing=TRUE)])
LDhub$Short_name2 <- sub("blank[1-9]", "", LDhub$Short_name)

# Plot correlation with dyslexia and confidence intervals for each trait
ldhub_plot <- ggplot(LDhub, aes(y = Short_name)) + 

# Add a point range for MTAG and CP results
  geom_pointrange(size = 1.5, fatten=2, aes(x = rg_CP, xmin = rg_CP - se_CP, xmax = rg_CP + se_CP, alpha=Significance_CP), color = viridis(n=3)[1], show.legend = FALSE) +
  geom_pointrange(size = 1.5, fatten=2, aes(x = rg_MTAG, xmin = rg_MTAG - se_MTAG, xmax = rg_MTAG + se_MTAG, alpha=Significance_MTAG), color = viridis(n=3)[2], show.legend = FALSE) +
  
  scale_y_discrete(labels=LDhub$Short_name2[order(LDhub$Order, decreasing=TRUE)]) +
  
  scale_alpha_discrete(range=c(0.25, 0.8)) +
# Add a line at zero to divide positive from negative correlations   
  scale_x_continuous(expand = c(0, 0), limits = c(-1.1,1.1), breaks = c(-1, -0.8, -0.6, -0.4, -0.2, 0.0, 0.2, 0.4, 0.6, 0.8, 1))+

# Change theme to classic (white background and no gridlines)
  theme_classic()+

  theme(# Increase thickness of x axis
        axis.line.x = element_line(colour = "black", size = 1),
		axis.line.y = element_line(colour = "black", size = 1),
        # Remove legend
        legend.position = "none",
		# Remove ticks from y axis
        axis.ticks.y = element_blank()) +
		# specify axis labels
		labs(x="Genetic correlation (rg)", y="") +
# use other column for y-axis labels

# add headings
	annotate(geom = "text", x = -1, y = 86.5, label = "Cognitive", hjust = 0, vjust = 1, size = 3, colour="grey31", fontface =2) +
	annotate(geom = "text", x = -1, y = 80.5, label = "Education", hjust = 0, vjust = 1, size = 3, colour="grey31", fontface =2) +
 	annotate(geom = "text", x = -1, y = 69.5, label = "Eyesight", hjust = 0, vjust = 1, size = 3, colour="grey31", fontface =2) +
	annotate(geom = "text", x = -1, y = 65.5, label = "Chronotype", hjust = 0, vjust = 1, size = 3, colour="grey31", fontface =2) +
	annotate(geom = "text", x = -1, y = 63.5, label = "Lifestyle", hjust = 0, vjust = 1, size = 3, colour="grey31", fontface =2) +
	annotate(geom = "text", x = -1, y = 49.5, label = "Wellbeing", hjust = 0, vjust = 1, size = 3, colour="grey31", fontface =2) +
	annotate(geom = "text", x = -1, y = 38.5, label = "Psychiatric", hjust = 0, vjust = 1, size = 3, colour="grey31", fontface =2) +
	annotate(geom = "text", x = -1, y = 34.5, label = "Pain", hjust = 0, vjust = 1, size = 3, colour="grey31", fontface =2) +
	annotate(geom = "text", x = -1, y = 26.5, label = "Physical health and exercise", hjust = 0, vjust = 1, size = 3, colour="grey31", fontface =2) +
	annotate(geom = "text", x = -1, y = 10.5, label = "SES", hjust = 0, vjust = 1, size = 3, colour="grey31", fontface =2) +
 
# Add horizontal lines to divide categories
    geom_segment(aes(x = -1, xend = 1, y = 11, yend = 11), colour = "grey70")+
    geom_segment(aes(x = -1, xend = 1, y = 27, yend = 27), colour = "grey70")+
    geom_segment(aes(x = -1, xend = 1, y = 35, yend = 35), colour = "grey70")+
    geom_segment(aes(x = -1, xend = 1, y = 39, yend = 39), colour = "grey70")+
    geom_segment(aes(x = -1, xend = 1, y = 50, yend = 50), colour = "grey70")+
    geom_segment(aes(x = -1, xend = 1, y = 64, yend = 64), colour = "grey70")+
    geom_segment(aes(x = -1, xend = 1, y = 66, yend = 66), colour = "grey70")+
    geom_segment(aes(x = -1, xend = 1, y = 70, yend = 70), colour = "grey70")+
    geom_segment(aes(x = -1, xend = 1, y = 81, yend = 81), colour = "grey70")+
	geom_segment(aes(x = 0, xend = 0, y = 0.5, yend = 86.5), colour = "grey31")
    
	
# View plot
ldhub_plot

# store plot
pdf(paste(DataDir, "Plot_MTAG_and_CP_LDhub_results_subset_viridis.pdf", sep=""), width=9, height=12)   
ldhub_plot
dev.off()

### Legend is added using illustrator


