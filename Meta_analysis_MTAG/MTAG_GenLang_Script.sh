#\!/bin/sh
#$ -N Run_MTAG
#$ -o /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Meta_analysis_MTAG/logs
#$ -e /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Meta_analysis_MTAG/logs
#$ -cwd
#$ -q single15.q
#$ -S /bin/bash
#$ -M else.eising@mpi.nl
#$ -m beas


# By Else Eising
# script to run MTAG

# **mtag: Multitrait Analysis of GWAS** This program is the implementation of
# MTAG method described by Turley et. al. Requires the input of a comma-
# separated list of GWAS summary statistics with identical columns. It is
# recommended to pass the column names manually to the program using the options
# below. The implementation of MTAG makes use of the LD Score Regression (ldsc)
# for cleaning the data and estimating residual variance-covariance matrix, so
# the input must also be compatible ./munge_sumstats.py command in the ldsc
# distribution included with mtag. The default estimation method for the genetic
# covariance matrix Omega is GMM (as described in the paper). Note below: any
# list of passed to the options below must be comma-separated without
# whitespace.

##=======================================
## Set paths and directories
##=======================================

MTAGdir="/home/elseis/mtag/"
WorkingDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_MTAG/"
MetaDir="/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/"

[[ -d $WorkingDir ]] || mkdir $WorkingDir
cd $WorkingDir

##=======================================
## Run MTAG
##=======================================

# I got error messages when:
# - mtag identified two columns with SNP IDs, even though specifying one
# - column with chromosome info was not present

$MTAGdir/mtag.py \
--sumstats $MetaDir/WR_RT/METAANALYSIS_WR_RT_EUR_combined_STERR_GCOFF_1_chrpos.txt,$MetaDir/NREAD_RT/METAANALYSIS_NREAD_RT_EUR_combined_STERR_GCOFF_1_chrpos.txt,$MetaDir/SP_RT/METAANALYSIS_SP_RT_EUR_combined_STERR_GCOFF_1_chrpos.txt,$MetaDir/PA_RT/METAANALYSIS_PA_RT_EUR_combined_STERR_GCOFF_1_chrpos.txt \
--fdr \
--out $WorkingDir/MTAG_WR_NREAD_SP_PA \
--snp_name MarkerName \
--beta_name Effect \
--se_name StdErr \
--n_name TotalSampleSize \
--eaf_name Freq1 \
--a1_name Allele1 \
--a2_name Allele2 \
--p_name P-value \
--chr_name CHR \
--bpos_name POS \
--use_beta_se \
--ld_ref_panel $MTAGdir/ld_ref_panel/eur_w_ld_chr/ \
--stream_stdout 

