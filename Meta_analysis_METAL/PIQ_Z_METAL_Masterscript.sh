# By Else Eising
# script to make the scripts for the analysis of PIQ in METAL

# Start by making the base script. This script should contain all input files

##=======================================
## Copy METAL to clusterfs
##=======================================

[[ -d /data/clusterfs/lag/users/elseis/GenLang/generic-metal/ ]] || cp -r /data/workspaces/lag/shared_spaces/Resource_DB/generic-metal/ /data/clusterfs/lag/users/elseis/GenLang/generic-metal/


##=======================================
## Define settings
##=======================================

Pheno="PIQ_Z"
WorkingDir="/data/clusterfs/lag/users/elseis/GenLang/"$Pheno"/"
Workspace="/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/"$Pheno"/"
Basescript=$WorkingDir/SCRIPTS/$Pheno.METAL.sh

[[ -d $WorkingDir ]] || mkdir $WorkingDir
[[ -d $WorkingDir/SCRIPTS/ ]] || mkdir $WorkingDir/SCRIPTS/


##=======================================
## Copy the summary stats to clusterfs
##=======================================

echo "/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Alspac/CLEANED.ALSPAC_EUR_PIQ_Z_COMBINED_20180503.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/ASTON/CLEANED.ASTON_EUR_PIQ_Z_COMBINED_20200628.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/BATS/CLEANED.BATS_EUR_PIQ_Z_COMBINED_lv1grm_202006.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/BCBL/CLEANED.BCBL_EUR_PIQ_Z_COMBINED_20200428.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/CLDRC/CLEANED.CLDRC_EUR_PIQ_Z_COMBINED_20200522.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/ELVS/CLEANED.ELVS_EUR_PIQ_Z_COMBINED_20200506.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Iowa/CLEANED.Iowa_EUR_PIQ_Z_COMBINED_20180507.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/NTR/CLEANED.NTR_EUR_PIQ_Z_COMBINED_20200708.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/RAINE/CLEANED.RAINE_EUR_PIQ_Z_COMBINED_20180309.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/SLIC/CLEANED.SLIC_EUR_PIQ_Z_COMBINED_20200522.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/SYS/CLEANED.SYS_EUR_PIQ_Z_COMBINED_20200430.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/TEDS/CLEANED.GenLang_data_full_sample_Affy.g12_F.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/TEDS/CLEANED.GenLang_data_full_sample_OEE.g12_F.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Toronto/CLEANED.Toronto_EUR_PIQ_Z_COMBINED_20200616_WISCIII.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Toronto/CLEANED.Toronto_EUR_PIQ_Z_COMBINED_20200616_WISC4.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/UKdys/CLEANED.UKdys_EUR_PIQ_Z_COMBINED_20200522.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/York/CLEANED.York_EUR_PIQ_Z_COMBINED_20200522.txt.gz
" > $WorkingDir/Input_file_list.txt



while read sumstat; do
s=$(echo $sumstat| sed 's|/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/[a-Z]*/||')
echo "$s"
if [ ! -e $WorkingDir/$s ];
	then
	cp $sumstat $WorkingDir/$s
	fi
done < $WorkingDir/Input_file_list.txt

##=======================================
## Make Base script
##=======================================

echo "# Script for the analysis of $Pheno in METAL
# This script was created by Else Eising with the "$Pheno"_METAL_Masterscript.sh in the above directory


# === GENERIC SETTINGS of input files === #
GENOMICCONTROL ON
SEPARATOR TAB
MARKER ID.ref
ALLELE EFFECT_ALLELE OTHER_ALLELE
FREQ EAF
EFFECT BETA
PVALUE PVAL 
STDERR SE
WEIGHT N
STRAND STRAND
USESTRAND ON
AVERAGEFREQ ON
MINMAXFREQ ON

# === SETTINGS for the analysis === #
SCHEME STDERR
CUSTOMVARIABLE TotalSampleSize   # only when scheme stderr, otherwise Weight will be in the output file
LABEL TotalSampleSize as N


# === PROCESS THE  INPUT FILES === #
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Alspac/CLEANED.ALSPAC_EUR_PIQ_Z_COMBINED_20180503.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/ASTON/CLEANED.ASTON_EUR_PIQ_Z_COMBINED_20200628.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/BATS/CLEANED.BATS_EUR_PIQ_Z_COMBINED_lv1grm_202006.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/BCBL/CLEANED.BCBL_EUR_PIQ_Z_COMBINED_20200428.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/CLDRC/CLEANED.CLDRC_EUR_PIQ_Z_COMBINED_20200522.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/ELVS/CLEANED.ELVS_EUR_PIQ_Z_COMBINED_20200506.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Iowa/CLEANED.Iowa_EUR_PIQ_Z_COMBINED_20180507.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/NTR/CLEANED.NTR_EUR_PIQ_Z_COMBINED_20200708.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/RAINE/CLEANED.RAINE_EUR_PIQ_Z_COMBINED_20180309.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/SLIC/CLEANED.SLIC_EUR_PIQ_Z_COMBINED_20200522.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/SYS/CLEANED.SYS_EUR_PIQ_Z_COMBINED_20200430.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/TEDS/CLEANED.GenLang_data_full_sample_Affy.g12_F.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/TEDS/CLEANED.GenLang_data_full_sample_OEE.g12_F.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Toronto/CLEANED.Toronto_EUR_PIQ_Z_COMBINED_20200616_WISCIII.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Toronto/CLEANED.Toronto_EUR_PIQ_Z_COMBINED_20200616_WISC4.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/UKdys/CLEANED.UKdys_EUR_PIQ_Z_COMBINED_20200522.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/York/CLEANED.York_EUR_PIQ_Z_COMBINED_20200522.txt.gz

# === ANALYSIS === #
OUTFILE $WorkingDir/METAANALYSIS_"$Pheno"_combined_STERR_GCON_ .tbl
ANALYZE HETEROGENEITY


CLEAR


QUIT

" > $Basescript

sed -i 's|/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/[a-Z]*/|'$WorkingDir'|g' $Basescript



##=======================================
## Make scripts
##=======================================

# GCOFF
sed -e 's/GENOMICCONTROL ON/GENOMICCONTROL OFF/' -e 's/_GCON_/_GCOFF_/' $Basescript > $WorkingDir/SCRIPTS/$Pheno.GCOFF_METAL.sh


##=======================================
## Make script to run all METAL scripts
##=======================================

ls $WorkingDir/SCRIPTS/*.sh > $WorkingDir/List_of_scripts.txt


echo "#\!/bin/sh
#$ -N METAL_"$Pheno"
#$ -cwd
#$ -q single.q
#$ -S /bin/bash
#$ -M else.eising@mpi.nl
#$ -m beas

" > $WorkingDir/Run_scripts_METAL.$Pheno.sh

while read script; do
s=$(echo $script| sed 's|'$WorkingDir'/SCRIPTS/||')
echo "/data/clusterfs/lag/users/elseis/GenLang/generic-metal/metal < _SCRIPT_ > _SCRIPTOUT_.run.log" | sed 's#_SCRIPT_#'$script'#g' | sed 's#_SCRIPTOUT_#'$WorkingDir/$s'#g'>> $WorkingDir/Run_scripts_METAL.$Pheno.sh
done < $WorkingDir/List_of_scripts.txt

# remove double slashes from script

echo "mv $WorkingDir/*.tbl.info $Workspace" >> $WorkingDir/Run_scripts_METAL.$Pheno.sh
echo "mv $WorkingDir/*.tbl $Workspace" >> $WorkingDir/Run_scripts_METAL.$Pheno.sh

sed -i 's|//|/|g' $WorkingDir/Run_scripts_METAL.$Pheno.sh






