# By Else Eising
# script to make the scripts for the analysis of nonword repetition in METAL
# Random effects meta-analysis, to work with the high heterogeneity in the results

# Start by making the base script. This script should contain all input files

##=======================================
## Copy METAL to clusterfs
##=======================================

[[ -d /data/clusterfs/lag/users/elseis/GenLang/random-metal/ ]] || cp -r /data/workspaces/lag/shared_spaces/Resource_DB/random-metal-master/ /data/clusterfs/lag/users/elseis/GenLang/random-metal/

##=======================================
## Define input files
##=======================================
GenReur="CLEANED.NWR_rank_eur_all_reorder_v5.gz"
GenRtotal="CLEANED.NWR_rank_combined_all_reorder_v5.gz"

##=======================================
## Define settings
##=======================================

Pheno="NREP_RT"
Workspace="/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/"$Pheno"/"
WorkingDir="/data/clusterfs/lag/users/elseis/GenLang/"$Pheno"/"

[[ -d $WorkingDir ]] || mkdir $WorkingDir
[[ -d $WorkingDir/SCRIPTS/ ]] || mkdir $WorkingDir/SCRIPTS/

Basescript=$WorkingDir/SCRIPTS/$Pheno.METAL.sh


##=======================================
## Copy the summary stats to clusterfs
##=======================================

echo "/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Alspac/CLEANED.ALSPAC_EUR_NRep_RT_COMBINED_20180503.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/BATS/CLEANED.BATS_EUR_NRep_RT_COMBINED_lv1grm_202006.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/BCBL/CLEANED.BCBL_EUR_NRep_RT_COMBINED_20200428.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/CLDRC/CLEANED.CLDRC_EUR_NRep_RT_COMBINED_20200522.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/ELVS/CLEANED.ELVS_EUR_NRep_RT_COMBINED_20200506.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/GenR/CLEANED.NWR_rank_eur_all_reorder_v5.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/GenR/CLEANED.NWR_rank_combined_all_reorder_v5.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Iowa/CLEANED.Iowa_EUR_NRep_RT_COMBINED_20180507.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/SLIC/CLEANED.SLIC_EUR_NRep_RT_COMBINED_20200522.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Toronto/CLEANED.Toronto_EUR_NRep_RT_COMBINED_20200616.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/York/CLEANED.York_EUR_NRep_RT_COMBINED_20200522.txt.gz
" > $WorkingDir/Input_file_list.txt


while read sumstat; do
s=$(echo $sumstat| sed 's|/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/[a-Z]*/||')
echo "$s"
if [ ! -e $WorkingDir/$s ];
	then
	cp $sumstat $WorkingDir/$s
	fi
done < $WorkingDir/Input_file_list.txt



##=======================================
## Make Base script
##=======================================

echo "# Script for the analysis of $Pheno in METAL
# This script was created by Else Eising with the "$Pheno"_METAL_Masterscript.sh


# === GENERIC SETTINGS of input files === #
GENOMICCONTROL ON
SEPARATOR TAB
MARKER ID.ref
ALLELE EFFECT_ALLELE OTHER_ALLELE
FREQ EAF
EFFECT BETA
PVALUE PVAL 
STDERR SE
WEIGHT N
STRAND STRAND
USESTRAND ON
AVERAGEFREQ ON
MINMAXFREQ ON

# === SETTINGS for the analysis === #
SCHEME STDERR
CUSTOMVARIABLE TotalSampleSize   # only when scheme stderr, otherwise Weight will be in the output file
LABEL TotalSampleSize as N


# === PROCESS THE  INPUT FILES === #
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Alspac/CLEANED.ALSPAC_EUR_NRep_RT_COMBINED_20180503.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/BATS/CLEANED.BATS_EUR_NRep_RT_COMBINED_lv1grm_202006.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/BCBL/CLEANED.BCBL_EUR_NRep_RT_COMBINED_20200428.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/CLDRC/CLEANED.CLDRC_EUR_NRep_RT_COMBINED_20200522.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/ELVS/CLEANED.ELVS_EUR_NRep_RT_COMBINED_20200506.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/GenR/CLEANED.NWR_rank_combined_all_reorder_v5.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Iowa/CLEANED.Iowa_EUR_NRep_RT_COMBINED_20180507.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/SLIC/CLEANED.SLIC_EUR_NRep_RT_COMBINED_20200522.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Toronto/CLEANED.Toronto_EUR_NRep_RT_COMBINED_20200616.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/York/CLEANED.York_EUR_NRep_RT_COMBINED_20200522.txt.gz

# === ANALYSIS === #
OUTFILE $WorkingDir/METAANALYSIS_RANDOM_"$Pheno"_combined_STERR_GCON_ .tbl
ANALYZE RANDOM


CLEAR


QUIT

" > $Basescript

sed -i 's|/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/[a-Z]*/|'$WorkingDir'|g' $Basescript




##=======================================
## Make other scripts
##=======================================


# European only
sed -e 's#'$GenRtotal'#'$GenReur'#' -e 's/_combined_STERR/_EUR_combined_STERR/' $Basescript > $WorkingDir/SCRIPTS/$Pheno.EUR_METAL.sh

# GCOFF
sed -e 's/GENOMICCONTROL ON/GENOMICCONTROL OFF/' -e 's/_GCON_/_GCOFF_/' $Basescript > $WorkingDir/SCRIPTS/$Pheno.GCOFF_METAL.sh

# GCOFF, European only
sed -e 's/GENOMICCONTROL ON/GENOMICCONTROL OFF/' -e 's/_GCON_/_GCOFF_/' $WorkingDir/SCRIPTS/$Pheno.EUR_METAL.sh > $WorkingDir/SCRIPTS/$Pheno.EUR_GCOFF_METAL.sh


##=======================================
## Make script to run all METAL scripts
##=======================================

ls $WorkingDir/SCRIPTS/*.sh > $WorkingDir/List_of_scripts.txt


echo "#\!/bin/sh
#$ -N METAL_"$Pheno"
#$ -cwd
#$ -q single.q
#$ -S /bin/bash
#$ -M else.eising@mpi.nl
#$ -m beas

" > $WorkingDir/Run_scripts_METAL.$Pheno.sh

while read script; do
s=$(echo $script| sed 's|'$WorkingDir'/SCRIPTS/||')
echo "/data/clusterfs/lag/users/elseis/GenLang/random-metal/executables/metal < _SCRIPT_ > _SCRIPTOUT_.run.log" | sed 's#_SCRIPT_#'$script'#g' | sed 's#_SCRIPTOUT_#'$WorkingDir/$s'#g'>> $WorkingDir/Run_scripts_METAL.$Pheno.sh
done < $WorkingDir/List_of_scripts.txt

# remove double slashes from script

echo "mv $WorkingDir/*.tbl.info $Workspace" >> $WorkingDir/Run_scripts_METAL.$Pheno.sh
echo "mv $WorkingDir/*.tbl $Workspace" >> $WorkingDir/Run_scripts_METAL.$Pheno.sh


sed -i 's|//|/|g' $WorkingDir/Run_scripts_METAL.$Pheno.sh

