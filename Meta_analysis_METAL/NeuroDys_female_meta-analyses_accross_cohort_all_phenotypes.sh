# By Else Eising
# script to make the scripts for the meta-analysis of the Neurodys cohort in METAL


##=======================================
## Copy METAL to clusterfs
##=======================================

[[ -d /data/clusterfs/lag/users/elseis/GenLang/generic-metal/ ]] || cp -r /data/workspaces/lag/shared_spaces/Resource_DB/generic-metal/ /data/clusterfs/lag/users/elseis/GenLang/generic-metal/


##=======================================
## Define settings
##=======================================

Workspace="/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/Neurodys/"
WorkingDir="/data/clusterfs/lag/users/elseis/GenLang/Neurodys/"

[[ -d $ScriptDir ]] || mkdir $ScriptDir
[[ -d $WorkingDir ]] || mkdir $WorkingDir
[[ -d $WorkingDir/SCRIPTS/ ]] || mkdir $WorkingDir/SCRIPTS/

Basescript=$WorkingDir/SCRIPTS/Neurodys.FEMALE.METAL.NRead.sh


##=======================================
## Copy the summary stats to clusterfs
##=======================================

echo "/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_AGS_NRead_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_AGS_PA_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_AGS_SP_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_AGS_WR_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_AGS_WR_Z_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_FIN_NRead_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_FIN_PA_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_FIN_SP_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_FIN_WR_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_FIN_WR_Z_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_FRA_NRead_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_FRA_PA_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_FRA_SP_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_FRA_WR_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_FRA_WR_Z_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_NL_NRead_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_NL_PA_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_NL_SP_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_NL_WR_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_NL_WR_Z_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_HUN_NRead_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_HUN_PA_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_HUN_SP_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_HUN_WR_RT_FEMALE_20181018.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_HUN_WR_Z_FEMALE_20181018.txt.gz
" > $WorkingDir/Input_file_list.txt


while read sumstat; do
s=$(echo $sumstat| sed 's|/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/[a-Z]*/||')
echo "$s"
if [ ! -e $WorkingDir/$s ];
	then
	cp $sumstat $WorkingDir/$s
	fi
done < $WorkingDir/Input_file_list.txt



##=======================================
## Make Base script
##=======================================

echo "# Script for the analysis of NRead in METAL
# This script was created by Else Eising with the NeuroDys_combined_meta-analyses_accross_cohort_all_phenotypes.sh script


# === GENERIC SETTINGS of input files === #
GENOMICCONTROL ON
SEPARATOR TAB
MARKER ID.ref
ALLELE EFFECT_ALLELE OTHER_ALLELE
FREQ EAF
EFFECT BETA
PVALUE PVAL 
STDERR SE
WEIGHT N
STRAND STRAND
USESTRAND ON
AVERAGEFREQ ON
MINMAXFREQ ON

# === SETTINGS for the analysis === #
SCHEME STDERR
CUSTOMVARIABLE N   # only when scheme stderr, otherwise Weight will be in the output file
LABEL N as N

# === PROCESS THE  INPUT FILES === #
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_AGS_NRead_RT_FEMALE_20181018.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_FIN_NRead_RT_FEMALE_20181018.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_FRA_NRead_RT_FEMALE_20181018.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_NL_NRead_RT_FEMALE_20181018.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_HUN_NRead_RT_FEMALE_20181018.txt.gz

# === ANALYSIS === #
OUTFILE $WorkingDir/METAANALYSIS_Neurodys_female_NRead_RT_STERR_GCON_ .tbl
ANALYZE HETEROGENEITY


CLEAR


QUIT

" > $Basescript

sed -i 's|/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/[a-Z]*/|'$WorkingDir'|g' $Basescript




##=======================================
## Make other scripts
##=======================================

# all phenotypes RT
for phenotype in WR PA SP
do
sed -e 's/NRead/'$phenotype'/' $Basescript > $WorkingDir/SCRIPTS/Neurodys.FEMALE.METAL.$phenotype.sh
done

# WR_Z
sed -e 's/rank/scale/' -e 's/_WR_RT_/_WR_Z_/' $WorkingDir/SCRIPTS/Neurodys.FEMALE.METAL.WR.sh > $WorkingDir/SCRIPTS/Neurodys.FEMALE.METAL.WR.Z.sh

# all phenotypes RT, GCOFF
sed -e 's/GENOMICCONTROL ON/GENOMICCONTROL OFF/' -e 's/_GCON_/_GCOFF_/' $Basescript > $WorkingDir/SCRIPTS/Neurodys.FEMALE.METAL.NRead.GCOFF.sh

for phenotype in WR PA SP
do
sed -e 's/NRead/'$phenotype'/' $WorkingDir/SCRIPTS/Neurodys.FEMALE.METAL.NRead.GCOFF.sh > $WorkingDir/SCRIPTS/Neurodys.FEMALE.METAL.$phenotype.GCOFF.sh
done

# WR_Z
sed -e 's/rank/scale/' -e 's/_WR_RT_/_WR_Z_/' $WorkingDir/SCRIPTS/Neurodys.FEMALE.METAL.WR.GCOFF.sh > $WorkingDir/SCRIPTS/Neurodys.FEMALE.METAL.WR.Z.GCOFF.sh



##=======================================
## Make script to run all METAL scripts
##=======================================

ls $WorkingDir/SCRIPTS/Neurodys.FEMALE.METAL.*.sh > $WorkingDir/List_of_scripts.txt


echo "#\!/bin/sh
#$ -N METAL_Neurodys
#$ -cwd
#$ -q single.q
#$ -S /bin/bash
#$ -M else.eising@mpi.nl
#$ -m beas

" > $WorkingDir/Run_scripts_METAL.Neurodys.FEMALE.sh

while read script; do
s=$(echo $script| sed 's|'$WorkingDir'/SCRIPTS/||')
echo "/data/clusterfs/lag/users/elseis/GenLang/generic-metal/metal < _SCRIPT_ > _SCRIPTOUT_.run.log" | sed 's#_SCRIPT_#'$script'#g' | sed 's#_SCRIPTOUT_#'$WorkingDir/$s'#g'>> $WorkingDir/Run_scripts_METAL.Neurodys.FEMALE.sh
done < $WorkingDir/List_of_scripts.txt

# remove double slashes from script

echo "mv $WorkingDir/METAANALYSIS_Neurodys_female*.tbl.info $Workspace" >> $WorkingDir/Run_scripts_METAL.Neurodys.FEMALE.sh
echo "mv $WorkingDir/METAANALYSIS_Neurodys_female*.tbl $Workspace" >> $WorkingDir/Run_scripts_METAL.Neurodys.FEMALE.sh


sed -i 's|//|/|g' $WorkingDir/Run_scripts_METAL.Neurodys.FEMALE.sh





