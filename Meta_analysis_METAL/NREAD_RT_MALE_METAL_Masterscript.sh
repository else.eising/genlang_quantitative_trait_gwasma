# By Else Eising
# script to make the scripts for the analysis of nonword reading in METAL

# Start by making the base script. This script should contain all input files


##=======================================
## Copy METAL to clusterfs
##=======================================

[[ -d /data/clusterfs/lag/users/elseis/GenLang/generic-metal/ ]] || cp -r /data/workspaces/lag/shared_spaces/Resource_DB/generic-metal/ /data/clusterfs/lag/users/elseis/GenLang/generic-metal/


##=======================================
## Define settings
##=======================================

Pheno="NREAD_RT"
Gender="MALE"
Workspace="/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/"$Pheno"_"$Gender"/"
WorkingDir="/data/clusterfs/lag/users/elseis/GenLang/"$Pheno"_"$Gender"/"

[[ -d $Workspace ]] || mkdir $Workspace
[[ -d $WorkingDir ]] || mkdir $WorkingDir
[[ -d $WorkingDir/SCRIPTS/ ]] || mkdir $WorkingDir/SCRIPTS/

Basescript=$WorkingDir/SCRIPTS/$Pheno.$Gender.METAL.sh


##=======================================
## Copy the summary stats to clusterfs
##=======================================

echo "/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Alspac/CLEANED.ALSPAC_EUR_NRead_RT_MALE_20180503.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/BATS/CLEANED.BATS_EUR_NRead_RT_MALE_lv1grm_202006.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/BCBL/CLEANED.BCBL_EUR_NRead_RT_MALE_20200428.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/CLDRC/CLEANED.CLDRC_EUR_NRead_RT_MALE_20200522.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/ELVS/CLEANED.ELVS_EUR_NRead_RT_MALE_20200506.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/FIOLA/CLEANED.FIOLA_EUR_NRead_RT_MALE_20200522.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/GRaD/CLEANED.GRaD_AA_NRead_RT_MALE_20180322.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/GRaD/CLEANED.GRaD_HA_NRead_RT_MALE_20180322.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Iowa/CLEANED.Iowa_EUR_NRead_RT_MALE_20180507.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/TEDS/CLEANED.GenLang_data_male_only_Affy.TOWREnonword_F.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/TEDS/CLEANED.GenLang_data_male_only_OEE.TOWREnonword_F.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Toronto/CLEANED.Toronto_EUR_NRead_RT_MALE_20201207.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/UKdys/CLEANED.UKdys_EUR_NRead_RT_MALE_20200522.txt.gz
/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/Neurodys/METAANALYSIS_Neurodys_male_NRead_RT_STERR_GCON_1.tbl
/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/Neurodys/METAANALYSIS_Neurodys_male_NRead_RT_STERR_GCOFF_1.tbl" > $WorkingDir/Input_file_list.txt



while read sumstat; do
s=$(echo $sumstat | sed 's|/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/[a-Z]*/||' | sed 's|/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/Neurodys/||')
echo "$s"
if [ ! -e $WorkingDir/$s ];
	then
	cp $sumstat $WorkingDir/$s
	fi
done < $WorkingDir/Input_file_list.txt



##=======================================
## Make Base script
##=======================================

echo "# Script for the analysis of "$Pheno"_"$Gender" in METAL
# This script was created by Else Eising with the "$Pheno"_"$Gender"_METAL_Masterscript.sh


# === GENERIC SETTINGS of input files === #
GENOMICCONTROL ON
SEPARATOR TAB
MARKER ID.ref
ALLELE EFFECT_ALLELE OTHER_ALLELE
FREQ EAF
EFFECT BETA
PVALUE PVAL 
STDERR SE
WEIGHT N
STRAND STRAND
USESTRAND ON
AVERAGEFREQ ON
MINMAXFREQ ON

# === SETTINGS for the analysis === #
SCHEME STDERR
CUSTOMVARIABLE TotalSampleSize   # only when scheme stderr, otherwise Weight will be in the output file
LABEL TotalSampleSize as N


# === PROCESS THE  INPUT FILES === #
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Alspac/CLEANED.ALSPAC_EUR_NRead_RT_MALE_20180503.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/BATS/CLEANED.BATS_EUR_NRead_RT_MALE_lv1grm_202006.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/BCBL/CLEANED.BCBL_EUR_NRead_RT_MALE_20200428.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/CLDRC/CLEANED.CLDRC_EUR_NRead_RT_MALE_20200522.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/ELVS/CLEANED.ELVS_EUR_NRead_RT_MALE_20200506.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/FIOLA/CLEANED.FIOLA_EUR_NRead_RT_MALE_20200522.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/GRaD/CLEANED.GRaD_AA_NRead_RT_MALE_20180322.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/GRaD/CLEANED.GRaD_HA_NRead_RT_MALE_20180322.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Iowa/CLEANED.Iowa_EUR_NRead_RT_MALE_20180507.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/TEDS/CLEANED.GenLang_data_male_only_Affy.TOWREnonword_F.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/TEDS/CLEANED.GenLang_data_male_only_OEE.TOWREnonword_F.txt.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Toronto/CLEANED.Toronto_EUR_NRead_RT_MALE_20201207.gz
PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/UKdys/CLEANED.UKdys_EUR_NRead_RT_MALE_20200522.txt.gz

# === GENERIC SETTINGS of Neurodys === #
MARKER MarkerName
ALLELE Allele1 Allele2
FREQ Freq1
EFFECT Effect
PVALUE P-value 
STDERR StdErr
USESTRAND OFF

PROCESS /data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/Neurodys/METAANALYSIS_Neurodys_male_NRead_RT_STERR_GCON_1.tbl

# === ANALYSIS === #
OUTFILE $WorkingDir/METAANALYSIS_"$Pheno"_"$Gender"_STERR_GCON_ .tbl
ANALYZE HETEROGENEITY


CLEAR


QUIT

" > $Basescript

sed -i 's|/data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/[a-Z]*/|'$WorkingDir'|g' $Basescript
sed -i 's|/data/workspaces/lag/workspaces/lg-genlang/Working/Meta_analysis_METAL/Neurodys/|'$WorkingDir'|g' $Basescript



##=======================================
## Make other scripts
##=======================================

# European
sed -e '/GRaD/d' -e 's/'$Gender'_STERR_GCON_/'$Gender'_EUR_STERR_GCON_/' $Basescript > $WorkingDir/SCRIPTS/$Pheno.$Gender.EUR_METAL.sh

# GCOFF
sed -e 's/GENOMICCONTROL ON/GENOMICCONTROL OFF/' -e 's/_GCON_/_GCOFF_/' $Basescript > $WorkingDir/SCRIPTS/$Pheno.$Gender.GCOFF_METAL.sh

# GCOFF, European
sed -e '/GRaD/d' -e 's/'$Gender'_STERR_GCOFF/'$Gender'_EUR_STERR_GCOFF/' $WorkingDir/SCRIPTS/$Pheno.$Gender.GCOFF_METAL.sh > $WorkingDir/SCRIPTS/$Pheno.$Gender.EUR_GCOFF_METAL.sh


##=======================================
## Make script to run all METAL scripts
##=======================================

ls $WorkingDir/SCRIPTS/*.sh > $WorkingDir/List_of_scripts.txt


echo "#\!/bin/sh
#$ -N METAL_"$Pheno"_"$Gender"
#$ -cwd
#$ -q single.q
#$ -S /bin/bash
#$ -M else.eising@mpi.nl
#$ -m beas

" > $WorkingDir/Run_scripts_METAL.$Pheno.$Gender.sh

while read script; do
s=$(echo $script| sed 's|'$WorkingDir'/SCRIPTS/||')
echo "/data/clusterfs/lag/users/elseis/GenLang/generic-metal/metal < _SCRIPT_ > _SCRIPTOUT_.run.log" | sed 's#_SCRIPT_#'$script'#g' | sed 's#_SCRIPTOUT_#'$WorkingDir/$s'#g'>> $WorkingDir/Run_scripts_METAL.$Pheno.$Gender.sh
done < $WorkingDir/List_of_scripts.txt

# remove double slashes from script

echo "mv $WorkingDir/METAANALYSIS_"$Pheno"_*.tbl.info $Workspace" >> $WorkingDir/Run_scripts_METAL.$Pheno.$Gender.sh
echo "mv $WorkingDir/METAANALYSIS_"$Pheno"_*.tbl $Workspace" >> $WorkingDir/Run_scripts_METAL.$Pheno.$Gender.sh


sed -i 's|//|/|g' $WorkingDir/Run_scripts_METAL.$Pheno.$Gender.sh





