#!/bin/sh
#$ -N Run_EasyQC
#$ -cwd
#$ -q single15.q
#$ -o /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_before_metaanalysis/logfiles/
#$ -e /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_before_metaanalysis/logfiles/
#$ -S /bin/bash
#$ -M else.eising@mpi.nl
#$ -m beas

# This script is used to run the EasyQC.R on the cluster.
R CMD BATCH /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_before_metaanalysis/EasyQC.R
