#!/bin/sh
#$ -N Run_EasyQC_per_pheno
#$ -cwd
#$ -q single15.q
#$ -S /bin/bash
#$ -M else.eising@mpi.nl
#$ -m beas


R CMD BATCH /data/workspaces/lag/workspaces/lg-genlang/Working/SCRIPTS/Genotype_data_QC_before_metaanalysis/Per_phenotype/EasyQC.R
