#################################################################################################################
##### EasyQC-script to perform study-level and meta-level QC on imputed 1000G data
##### EasyQC version: 9.0
##### Programmer: Thomas Winkler, 2014-09-22
##### Contact: thomas.winkler@klinik.uni-regensburg.de
#################################################################################################################

# Adapted by Else Eising, 28-05-2018
# to run script, type, use the EasyQC.R and EasyQC.sh scripts

### Please DEFINE here format and input columns of the following EASYIN files
DEFINE	--pathOut /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Per_phenotype/MALE/
		--strMissing NA
		--strSeparator TAB
		--acolIn cptid;ID.ref;CHR;POS;STRAND;EFFECT_ALLELE;OTHER_ALLELE;HWE_P;EAF;MAC;N;BETA;SE;PVAL;IMPUTED;INFO_TYPE;INFO
		--acolInClasses character;character;character;integer;character;character;character;numeric;numeric;numeric;numeric;numeric;numeric;numeric;integer;integer;numeric
		--acolNewName cptid;SNP;CHR;POS;STRAND;EFFECT_ALLELE;OTHER_ALLELE;HWE_P;EAF;MAC;N;BETA;SE;PVAL;IMPUTED;INFO_TYPE;INFO

## Please DO NOT CHANGE --acolNewName values because these reflect the column names used throughout the script
## If the study used different column names, please amend the respective value at --acolIn, the column will then 
## be automatically renamed to the respective --acolNewName value

### Please DEFINE here all input files:

EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/ABCD/CLEANED.ABCD_EUR_WR_Z_MALE_20200520.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/ABCD/CLEANED.ABCD_ALL_WR_Z_MALE_20200520.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Alspac/CLEANED.ALSPAC_EUR_WR_Z_MALE_20180503.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/ASTON/CLEANED.ASTON_EUR_WR_Z_MALE_20200628.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/BATS/CLEANED.BATS_EUR_WR_Z_MALE_lv1grm_202006.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/BCBL/CLEANED.BCBL_EUR_WR_Z_MALE_20200428.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/CLDRC/CLEANED.CLDRC_EUR_WR_Z_MALE_20200522.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/ELVS/CLEANED.ELVS_EUR_WR_Z_MALE_20200506.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/FIOLA/CLEANED.FIOLA_EUR_WR_Z_MALE_20200522.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/GRaD/CLEANED.GRaD_AA_WR_Z_MALE_20180322.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/GRaD/CLEANED.GRaD_HA_WR_Z_MALE_20180322.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Iowa/CLEANED.Iowa_EUR_WR_Z_MALE_20180507.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_AGS_WR_Z_MALE_20181018.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_FIN_WR_Z_MALE_20181018.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_FRA_WR_Z_MALE_20181018.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_NL_WR_Z_MALE_20181018.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Neurodys/CLEANED.Neurodys_HUN_WR_Z_MALE_20181018.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/NTR/CLEANED.NTR_EUR_WR_Z_MALE_20200708.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/PING/CLEANED.PING_EUR_WR_Z_MALE_20200522.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/PING/CLEANED.PING_ALL_WR_Z_MALE_20200522.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/PNC/CLEANED.PNC_EUR_WR_Z_MALE_20190405.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/PNC/CLEANED.PNC2_EUR_WR_Z_MALE_20190405.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/SLIC/CLEANED.SLIC_EUR_WR_Z_MALE_20200522.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/TEDS/CLEANED.GenLang_data_male_only_Affy.TOWREword_F.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/TEDS/CLEANED.GenLang_data_male_only_OEE.TOWREword_F.txt.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/Toronto/CLEANED.Toronto_EUR_WR_Z_MALE_20201207.gz
EASYIN	--fileIn  /data/workspaces/lag/workspaces/lg-genlang/Working/Genotypes_QC_before_metaanalysis/UKdys/CLEANED.UKdys_EUR_WR_Z_MALE_20200522.txt.gz


#################################################################################################################
## EASYQC Scripting interface:
START EASYQC

####################
## 9.  QQ plot

QQPLOT	--acolQQPlot PVAL
		--numPvalOffset 0.05
		--strMode subplot

####################
## 10. Summary Stats post-QC

CALCULATE --rcdCalc max(N,na.rm=T) --strCalcName N_max
GC	--colPval PVAL --blnSuppressCorrection 1

RPLOT	--rcdRPlotX N_max
		--rcdRPlotY Lambda.PVAL.GC
		--arcdAdd2Plot abline(h=1,col='orange');abline(h=1.1,col='red')
		--strAxes lim(0,NULL,0,NULL)
		--strPlotName GC-PLOT

####################
## 11. SE-N Plot - Trait transformation

CALCULATE --rcdCalc median(SE,na.rm=T) --strCalcName SE_median
CALCULATE --rcdCalc median(1/sqrt(2*EAF*(1-EAF)), na.rm=T) --strCalcName c_trait_transf

RPLOT 	--rcdRPlotX sqrt(N_max)
		--rcdRPlotY c_trait_transf/SE_median
		--arcdAdd2Plot abline(0,1,col='orange')
		--strAxes zeroequal
		--strPlotName SEN-PLOT
		
STOP EASYQC
#################################################################################################################
#################################################################################################################
